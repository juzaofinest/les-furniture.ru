const gulp = require('gulp');
const browserify = require('gulp-browserify');
const concat = require('gulp-concat');
const stylus = require('gulp-stylus');
const pug = require('gulp-pug');
const dest = require('gulp-dest');
const logger = require('gulp-logger');
const refresh = require('gulp-livereload');
const lr = require('tiny-lr');
const server = lr();
const minifyCSS = require('gulp-clean-css');
const embedlr = require('gulp-embedlr');

// gulp.task('scripts', () => {
//   gulp.src(['app/src/**/*.js'])
//     .pipe(browserify())
//     .pipe(concat('dest.js'))
//     .pipe(gulp.dest('dist/build'))
//     .pipe(refresh(server));
// });

gulp.task('styles', () => {
  gulp.src(['src/stylus/fonts.css', 'src/**/*.styl'])
    .pipe(stylus().on('error', console.log))
    .pipe(concat('site.css'))
    .pipe(gulp.dest('frontend/web/css'))
    .pipe(minifyCSS())
    .pipe(concat('site.min.css'))
    .pipe(gulp.dest('frontend/web/css'))
    .pipe(refresh(server));
});

const lrServerTask = () => {
  server.listen(35729, function(err) {
    if (err) return console.log(err);
  });
};
gulp.task('lr-server', lrServerTask);

const pugTask = gulp.task('pug', () => {
  gulp.src("src/views/**/*.pug")
    .pipe(embedlr())
    .pipe(logger())
    .pipe(pug({
      pretty: true,
      basedir: process.cwd(),
    }).on('error', console.log) )
    .pipe(dest('views', { ext:'.php' }))
    .pipe(gulp.dest('./frontend'))
    .pipe(refresh(server));
});

gulp.task('default', ['lr-server', 'pug', 'styles'], () => {
  // gulp.run();
  // gulp.run('lr-server', 'scripts', 'styles', 'html');
  // gulp.watch('app/src/**', function(event) {
  //   gulp.run('scripts');
  // });

  gulp.watch('src/**/*.styl', ['styles']);
  gulp.watch('src/views/**/*.pug', ['pug']);

});
