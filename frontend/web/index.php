<?php
require __DIR__ . '/../env.php';

$config = require __DIR__ . '/../config/web.php';

(new yii\web\Application($config))->run();
