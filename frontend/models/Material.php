<?php

namespace app\models;

use Yii;

/**
 * @property integer $id
 * @property string $description_ru
 * @property string $description_en
 */
class Material extends \yii\base\Object {
  public $id;
  public $description_ru;
  public $description_en;

  public static $entities = [
    '1'  => [
      'id'             => '1',
      'description_ru' => 'Акация',
      'description_en' => 'Acacia',
    ],
    '2'  => [
      'id'             => '2',
      'description_ru' => 'Клен',
      'description_en' => 'Maple',
    ],
    '3'  => [
      'id'             => '3',
      'description_ru' => 'Карагач',
      'description_en' => 'Elm',
    ],
    '4'  => [
      'id'             => '4',
      'description_ru' => 'Каштан',
      'description_en' => 'Chestnut',
    ],
    '5'  => [
      'id'             => '5',
      'description_ru' => 'Орех',
      'description_en' => 'Wallnut',
    ],
    '6'  => [
      'id'             => '6',
      'description_ru' => 'Груша',
      'description_en' => 'Pear',
    ],
    '7'  => [
      'id'             => '7',
      'description_ru' => 'Дуб',
      'description_en' => 'oak',
    ],
    '8'  => [
      'id'             => '8',
      'description_ru' => 'Мореный дуб',
      'description_en' => 'Bog oak',
    ],
    '9'  => [
      'id'             => '9',
      'description_ru' => 'Кедр',
      'description_en' => 'Cedar',
    ],
    '10' => [
      'id'             => '10',
      'description_ru' => 'Бук',
      'description_en' => 'Beech',
    ],
    '11' => [
      'id'             => '11',
      'description_ru' => 'Тополь',
      'description_en' => 'Poplar (cottonwood)',
    ],
    '12' => [
      'id'             => '12',
      'description_ru' => 'Ясень',
      'description_en' => 'Ash',
    ],
  ];

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['description_ru', 'description_en'], 'string', 'max' => 32],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id'             => Yii::t('app', 'ID'),
      'description_ru' => Yii::t('site', 'Material'),
      'description_en' => Yii::t('site', 'Material'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProducts() {
    return Product::find()->where(['material_id' => 'id']);
  }

  public static function findIdentity($id) {
    return isset(self::$entities[$id]) ? new static(self::$entities[$id]) : null;
  }

  public static function all() {
    $res = [];
    foreach (self::$entities as $value) {
      $res[] = self::findIdentity($value['id']);
    }
    return $res;
  }
}
