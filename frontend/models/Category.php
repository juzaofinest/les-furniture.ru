<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property integer $id
 * @property string $description_ru
 * @property string $description_en
 */
class Category extends \yii\db\ActiveRecord {
  /**
   * @inheritdoc
   */
  public static function tableName() {
    return '{{%categories}}';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['description_ru', 'description_en'], 'string', 'max' => 32],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id'             => Yii::t('app', 'ID'),
      'description_ru' => Yii::t('site', 'Category'),
      'description_en' => Yii::t('site', 'Category'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProducts() {
    return $this->hasMany(Product::className(), ['category_id' => 'id']);
  }
}
