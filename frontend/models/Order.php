<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%order}}".
 *
 * @property integer $id
 * @property string $firstname
 * @property string $phone
 * @property string $email
 * @property string $payment_method
 * @property string $cart
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['firstname', 'phone', 'payment_method'], 'required'],
            [['cart'], 'string'],
            [['firstname', 'email', 'payment_method'], 'string', 'max' => 64],
            [['phone'], 'string', 'max' => 32],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'firstname' => Yii::t('app', 'Firstname'),
            'phone' => Yii::t('app', 'Phone'),
            'email' => Yii::t('app', 'Email'),
            'payment_method' => Yii::t('app', 'Payment Method'),
            'cart' => Yii::t('app', 'Cart'),
        ];
    }
}
