<?php

namespace app\models;

use Yii;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;

/**
 * This is the model class for table "{{%products}}".
 *
 * @property integer $id
 * @property string $sku
 * @property string $model_ru
 * @property string $model_en
 * @property integer $category_id
 * @property integer $price
 * @property integer $quantity
 * @property integer $length
 * @property integer $height
 * @property integer $thickness
 * @property integer $sort_order
 *
 * @property Categories $category
 */
class Product extends \yii\db\ActiveRecord implements CartPositionInterface {
  use CartPositionTrait;
  /**
   * @inheritdoc
   */

  public static function tableName() {
    return '{{%products}}';
  }

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['category_id', 'cut_id', 'material_id', 'price', 'quantity', 'sort_order'], 'integer'],
      [['sku_ru', 'sku_en'], 'required'],
      [['sku_ru', 'sku_en', 'model_ru', 'model_en'], 'string', 'max' => 64],
      [['length', 'height', 'thickness'], 'string', 'max' => 32],
      [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],

      [['status', 'material_id', 'cut_id'], 'required'],
      ['status_id', 'in', 'range' => array_keys(Status::$entities)],
      ['cut_id', 'in', 'range' => array_keys(Cut::$entities)],
      ['material_id', 'in', 'range' => array_keys(Material::$entities)],
    ];
  }

  public function behaviors() {
    return [
      'image' => [
        'class' => 'rico\yii2images\behaviors\ImageBehave',
      ],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id'          => Yii::t('app', 'ID'),
      'model_ru'    => Yii::t('app', 'Model Ru'),
      'model_en'    => Yii::t('app', 'Model En'),
      'category_id' => Yii::t('app', 'Category ID'),
      'price'       => Yii::t('app', 'Price'),
      'quantity'    => Yii::t('app', 'Quantity'),
      'length'      => Yii::t('app', 'Length'),
      'height'      => Yii::t('app', 'Height'),
      'weight'      => Yii::t('app', 'Weight'),
      'sort_order'  => Yii::t('app', 'Sort Order'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getCategory() {
    return $this->hasOne(Category::className(), ['id' => 'category_id']);
  }

  public function getMaterial() {
    return Material::findIdentity($this->material_id);
  }

  public function getStatus() {
    return Status::findIdentity($this->status_id);
  }

  public function getCut() {
    return Cut::findIdentity($this->cut_id);
  }

  public function getPrice() {
    return $this->price;
  }

  public function getId() {
    return $this->id;
  }
}