<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * 
 */
class CheckoutForm extends Model
{
    public $firstname;
    public $email;
    public $phone;
    public $payment_method;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['firstname', 'email', 'phone', 'payment_method'], 'required'],
            // rememberMe must be a boolean value
            ['email', 'email'],
            [['firstname', 'email', 'payment_method'], 'string', 'max' => 64],
            [['phone'], 'string', 'max' => 32],
        ];
    }
	
	public function init()
	{
		parent::init();
		$cookies = Yii::$app->request->cookies;
		$arr = array();
		$arr['firstname'] = $cookies->getValue('firstname', '');
		$arr['phone'] = $cookies->getValue('phone', '');
		$arr['email'] = $cookies->getValue('email', '');
		$arr['payment_method'] = $cookies->getValue('payment_method', '');
		$this->setAttributes($arr);
	}
	
	public function afterValidate()
	{
		$cookies = Yii::$app->response->cookies;
		$attrs = $this->getAttributes(['firstname', 'phone', 'email', 'payment_method']);
		foreach ($attrs as $k => $v) {
	        $cookie = new \yii\web\Cookie([
	            'name' => $k,
	            'value' => $v,
	            'expire' => time() + 86400 * 360,
	        ]);
	
	        Yii::$app->response->cookies->add($cookie);
		}
		parent::afterValidate();
	}
}
