<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "{{%categories}}".
 *
 * @property integer $id
 * @property string $description_ru
 * @property string $description_en
 */
class Cut extends \yii\base\Object {
  public $id;
  public $description_ru;
  public $description_en;

  public static $entities = [
    '1' => [
      'id'             => '1',
      'description_ru' => 'Продольный',
      'description_en' => 'Lateral',
    ],
    '2' => [
      'id'             => '2',
      'description_ru' => 'Поперечный',
      'description_en' => 'Transversal',
    ],
  ];

  /**
   * @inheritdoc
   */
  public function rules() {
    return [
      [['description_ru', 'description_en'], 'string', 'max' => 32],
    ];
  }

  /**
   * @inheritdoc
   */
  public function attributeLabels() {
    return [
      'id'             => Yii::t('app', 'ID'),
      'description_ru' => Yii::t('app', 'Cut'),
      'description_en' => Yii::t('app', 'Cut'),
    ];
  }

  /**
   * @return \yii\db\ActiveQuery
   */
  public function getProducts() {
    return Product::find()->where(['cut_id' => 'id']);
  }

  public static function findIdentity($id) {
    return isset(self::$entities[$id]) ? new static(self::$entities[$id]) : null;
  }

  public static function all() {
    $res = [];
    foreach (self::$entities as $value) {
      $res[] = self::findIdentity($value['id']);
    }
    return $res;
  }
}
