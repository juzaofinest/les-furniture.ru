<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;

class ProductFilter extends Model {
  public $material;
  public $moisture;
  public $cut;
  public $category_id;
  public $price_from;
  public $price_to;
  public $width_from;
  public $width_to;
  public $length_from;
  public $length_to;
  public $thickness;

  public function rules() {
    return [
      [['material', 'moisture', 'category_id', 'cut', 'price_from', 'price_to', 'width_from', 'width_to', 'length_from', 'length_to', 'thickness'], 'integer'],
    ];
  }

  public function formName() {
    return '';
  }

  public function search() {
    $query = Product::find();

    // add conditions that should always apply here

    $dataProvider = new ActiveDataProvider([
      'query' => $query,
    ]);

    if (!$this->validate()) {
      return $dataProvider;
    }

    $query->andFilterWhere([
      'material_id' => $this->material,
      'category_id' => $this->category_id,
      'cut_id'      => $this->cut,
    ]);
    $query->andFilterWhere(['>=', 'price', $this->price_from]);
    $query->andFilterWhere(['<=', 'price', $this->price_to]);
    $query->andFilterWhere(['>=', 'length', $this->length_from]);
    $query->andFilterWhere(['<=', 'length', $this->length_to]);
    $query->andFilterWhere(['=', 'thickness', $this->thickness]);
    // $query->andFilterWhere(['<=', 'thickness', $this->thickness_to]);
    $query->andFilterWhere(['>=', 'width_ceil', $this->width_from]);
    $query->andFilterWhere(['<=', 'width_floor', $this->width_to]);

    if ($this->moisture) {
      $query->andFilterWhere([$this->moisture == 2 ? '<=' : '>=', 'moisture', 20]);
    }

    // edump($query);

    return $dataProvider;
  }

  public static function prices() {
    return [
      '5000'   => '5000',
      '10000'  => '10000',
      '20000'  => '20000',
      '30000'  => '30000',
      '40000'  => '40000',
      '50000'  => '50000',
      '60000'  => '60000',
      '70000'  => '70000',
      '80000'  => '80000',
      '90000'  => '90000',
      '100000' => '100000'];
  }

  public function pricesTo() {
    return array_filter(self::prices(), function ($i) {return $i >= $this->price_from;});
  }

  public function pricesFrom() {
    return $this->price_to
    ? array_filter(self::prices(), function ($i) {return $i <= $this->price_to;})
    : self::prices();
  }

  public static function steppy($min, $max, $step) {
    $arr = [];

    while ($min <= $max) {
      $arr[$min] = $min;
      $min += $step;
    }

    return $arr;
  }

  public function lengthsTo() {
    return array_filter(self::steppy(1000, 4500, 100), function ($i) {return $i >= $this->length_from;});
  }

  public function lengthsFrom() {
    return $this->length_to
    ? array_filter(self::steppy(1000, 4500, 100), function ($i) {return $i <= $this->length_to;})
    : self::steppy(1000, 4500, 100);
  }

  public function widthsTo() {
    return array_filter(self::steppy(200, 1200, 100), function ($i) {return $i >= $this->width_from;});
  }

  public function widthsFrom() {
    return $this->width_to
    ? array_filter(self::steppy(200, 1200, 100), function ($i) {return $i <= $this->width_to;})
    : self::steppy(200, 1200, 100);
  }

  public function thicknesses() {
    $arr = [];
    $p = Product::find()->select('DISTINCT `thickness`')->column();
    sort($p);
    foreach ($p as $value) {
      $arr[$value] = $value;
    }

    return $arr;
  }

  // public function thicknessesTo() {
  //   return array_filter(self::steppy(20, 250, 10), function ($i) {return $i >= $this->thickness_from;});
  // }
  // public function thicknessesFrom() {
  //   return $this->thickness_to
  //   ? array_filter(self::steppy(20, 250, 10), function ($i) {return $i <= $this->thickness_to;})
  //   : self::steppy(20, 250, 10);
  // }
}
