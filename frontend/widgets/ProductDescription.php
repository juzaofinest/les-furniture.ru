<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\Html;

class ProductDescription extends Widget {
  protected $message;
  protected $description = [];
  public $model;

  public function init() {
    parent::init();
  }

  public function run() {
    $model = $this->model;

    if ($model->{'sku' . Yii::t('site', '_en')}) {
      $this->description[] = Html::tag(
        'div',
        Yii::t('site', 'SKU') . ': ' . $model->{'sku' . Yii::t('site', '_en')},
        ['class' => 'spec text-grey']
      );
    }

    if ($model->material) {
      $this->description[] = Html::tag(
        'div',
        Yii::t('site', 'Material') . ': ' . $model->material->{Yii::t('site', 'description_en')},
        ['class' => 'spec text-grey']
      );
    }

    if ($model->cut) {
      $this->description[] = Html::tag(
        'div',
        Yii::t('site', 'Cut') . ': ' . $model->cut->{Yii::t('site', 'description_en')},
        ['class' => 'spec text-grey']
      );
    }

    if ($model->moisture) {
      $description = '10%';

      if ($model->moisture > 10) {
        $description = Yii::t('site', 'Wet');
      }

      if ($model->moisture < 10) {
        $description = Yii::t('site', 'Dry') . ' (<9%)';
      }

      $this->description[] = Html::tag(
        'div',
        Yii::t('site', 'Moisture') . ': ' . $description,
        ['class' => 'spec text-grey']
      );
    }

    return implode('', $this->description);
  }
}
