<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;

class ProductDimensions extends Widget {
  protected $message;
  protected $dimenstions = [];
  public $model;

  public function init() {
    parent::init();
  }

  public function run() {
    $model = $this->model;

    if ($model->height) {
      $this->dimenstions[] = Yii::t('site', 'Height') . ': ' . $model->height . ' ' . Yii::t('site', 'mm');
    }

    $this->addWidth($model);

    if ($model->length) {
      $this->dimenstions[] = Yii::t('site', 'Length') . ': ' . $model->length . ' ' . Yii::t('site', 'mm');
    }

    if ($model->thickness) {
      $this->dimenstions[] = Yii::t('site', 'Thickness') . ': ' . $model->thickness . ' ' . Yii::t('site', 'mm');
    }

    $this->message = implode('.<br>', $this->dimenstions);

    if ($this->message) {
      $this->message .= '.';
    }

    return $this->message;
  }

  protected function addWidth($model) {
    $a = $model->width_floor;
    $b = $model->width_ceil;

    if ($a && $b) {
      if ($a != $b) {
        $this->dimenstions[] = Yii::t('site', 'Width') . ": $a - $b " . Yii::t('site', 'mm');
      } else {
        $this->dimenstions[] = Yii::t('site', 'Width') . ": $a " . Yii::t('site', 'mm');
      }
    } else {
      $this->dimenstions[] = Yii::t('site', 'Width') . ": $a " . Yii::t('site', 'mm');
    }
  }
}
