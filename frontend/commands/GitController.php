<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;

class GitController extends Controller {
  public function actionClearDsStore() {
    $files = \yii\helpers\BaseFileHelper::findFiles(\Yii::getAlias('@app' . '/../'), []);
    $a = [];

    if ($files) {
      foreach ($files as $k => $file) {
        if (preg_match('/\.DS_Store$/', $file)) {
          $a[] = $file;
          unlink($file);
          echo (PHP_EOL . 'removed f ' . $file);
        }
      }
    }

    echo (PHP_EOL);
    return 0;
  }

  public function actionClearVendor() {
    $files = \yii\helpers\BaseFileHelper::findFiles(\Yii::getAlias('@app' . '/vendor'), []);

    foreach ($files as $k => $file) {
      if (preg_match('/\.git\//', $file)) {
        try {
          $dir = pathinfo($file, PATHINFO_DIRNAME);
          @\yii\helpers\BaseFileHelper::removeDirectory($dir);
          echo (PHP_EOL . 'removed d ' . $dir);
        } catch (Controller $e) {}
      } elseif (preg_match('/\.git[^\/]+$/', $file)) {
        try {
          unlink($file);
          echo (PHP_EOL . 'removed f ' . $file);
        } catch (Controller $e) {}
      } elseif (preg_match('/\.git[^\/]+\//', $file)) {
        try {
          $dir = pathinfo($file, PATHINFO_DIRNAME);
          @\yii\helpers\BaseFileHelper::removeDirectory($dir);
          echo (PHP_EOL . 'removed d ' . $dir);
        } catch (Controller $e) {}
      }
    }

    $a = [];
    $files = \yii\helpers\BaseFileHelper::findFiles(\Yii::getAlias('@app' . '/vendor'), []);

    foreach ($files as $k => $file) {
      if (preg_match('/git/', $file)) {
        $a[] = $file;
      }
    }

    echo (PHP_EOL);
    return 0;
  }
}
