<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\data\ArrayDataProvider;
use app\models\Order;
use app\models\Product;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yz\shoppingcart\ShoppingCart;
use app\models\CheckoutForm;

class CartController extends Controller
{
	public function behaviors()
	{
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['logout'],
				'rules' => [
					[
						'actions' => ['logout'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
			'verbs' => [
				'class' => VerbFilter::className(),
				'actions' => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actionView()
	{
		$cart = new ShoppingCart();
		$positionsDataProvider = new ArrayDataProvider([
			'allModels' => $cart->positions,
			'key' => 'id',
			'pagination' => [
				'pageSize' => 0,
			],
		]);
		
		$form = new CheckoutForm();
		
		if ($form->load(Yii::$app->request->post()) && $form->validate()) {
			$order = new Order();
			$attributes = $form->getAttributes();
			$attributes['cart'] = $cart->getSerialized();
			$order->setAttributes($attributes);
			if ($order->save()) {
				$this->sendReportEmail($cart, $order);
				if ($form->payment_method == 'cash'){
					return $this->redirect(['success'] + $order->getAttributes(['id', 'firstname']));
				}
			}			
		}

		return $this->render('view',[
			'cart' => $cart,
			'positionsDataProvider' => $positionsDataProvider,
			'cform' => $form,
		]);
	}

	public function actionSuccess()
	{
		$cart = new ShoppingCart();
		$cart->removeAll();
		
		$order_id = Yii::$app->request->get('id');
		$firstname = Yii::$app->request->get('firstname');

		return $this->render('success',[
			'order_id' => $order_id,
			'firstname' => $firstname,
		]);
	}

	public function actionRemoveall()
	{
		$cart = new ShoppingCart();
		$cart->removeAll();		
		return $this->redirect('view');
	}

	public function actionUpdateposition($id, $q)
	{
		$cart = new ShoppingCart();
		$model = Product::findOne($id);
		$cart->update($model, $q);	
		return $this->redirect('/cart/view');
	}
	
	protected function sendSuccessEmail($cart, $order)
	{
		Yii::$app->mailer->compose()
		    ->setFrom('info@les-furniture.ru')
		    ->setTo($order->getAttribute('email'))
		    ->setSubject('Подтверждение заказа ' .$order->getAttribute('id'))
		    ->setTextBody('Plain text content')
		    ->send();		
		
	}
	
	protected function sendReportEmail($cart, $order)
	{
		$i = 1;
		$text = '';
		foreach ($cart->getPositions() as $p){
			$text .= $i .'. ' .$p->getAttribute('model_ru') .'. Количество: ' .$p->getQuantity() .PHP_EOL
			.Url::to(['product/view', 'id' => $p->getAttribute('id')], true) .PHP_EOL;
			$i++;
		}
		$text .= 'Итого: ' .$cart->getCost() .' руб.';
		
		Yii::$app->mailer->compose()
		    ->setFrom('order@les-furniture.ru')
		    ->setTo('info@les-furniture.ru')
		    ->setSubject('Получен новый заказ №'.$order->getAttribute('id'))
		    ->setTextBody('Имя клиента: ' .$order->getAttribute('firstname') .PHP_EOL 
			    .'Телефон клиента: ' .$order->getAttribute('phone') .PHP_EOL
			    .'E-mail клиента: ' .$order->getAttribute('email') .PHP_EOL
			    .'Способ оплаты: ' .$order->getAttribute('payment_method') .PHP_EOL .$text
		    )
		    ->send();		
		
	}
}
