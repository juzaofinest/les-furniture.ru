<?php
//! ЭТО НЕ КОНТРОЛЛЕР МОДЕЛИ IMAGE
namespace app\controllers;

use Yii;
use app\models\Product;
use rico\yii2images\models\Image;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ImageController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['setmain', 'deleteajax', 'sort'],
                'rules' => [
                    [
                        'actions' => ['setmain', 'deleteajax', 'sort'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
		return 1;
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDeleteajax()
    {
		$request = Yii::$app->request->post();
		if (isset($request['id'])) {
			$id = $request['id'];
		} else {
			return false;
		}
		$image = Image::findOne($id);
		
		if($image) {
			$modelName = 'app\models\\'.$image->modelName;
			$modelId = $image->itemId;
			$model = new $modelName;
			$model = $model->findOne($modelId);
			$model->removeImage($image); return true;
		} else {
			return false;
		}
    }


    public function actionSetmain()
    {
		$request = Yii::$app->request->post();
		if (isset($request['id'])) {
			$id = $request['id'];
		} else {
			return false;
		}
		$image = Image::findOne($id);
		
		if($image) {
			$modelName = 'app\models\\'.$image->modelName;
			$modelId = $image->itemId;
			$model = new $modelName;
			$model = $model->findOne($modelId);
			$model->setMainImage($image); return true;
		} else {
			return false;
		}
    }
//! чет не работает
    public function actionLastimage()
    {
		$request = Yii::$app->request->post();
		if (isset($request['modelId'])) {
			$modelId = $request['modelId'];
		} else {
			return false;
		}
		$modelName = 'app\models\\'.'Product';
		$model = new $modelName;
		$model = $model->findOne($modelId);
		
		$images = $model->getImages();
		$image = array_pop($images);

		if($image) {
			return $this->actionSingle($image->id);
		} else {
			return false;
		}
    }

    public function actionSingle($id = null)
    {
		$request = Yii::$app->request->post();
		if (!$id && !isset($request['id'])) {
			return false;
		} else if (!$id && isset($request['id'])){
			$id = $request['id'];
		}
		$image = Image::findOne($id);
		
		if($image) {
            return $this->renderPartial('image-block', [
                'p_img' => $image,
            ]);
		} else {
			return false;
		}
    }

    public function actionSort()
    {
		$request = Yii::$app->request->post();
		if (!isset($request['new_order'])) {
			return false;
		}
		
		$new_order = $request['new_order'];
		$new_order = (array)explode('&', $new_order);
		if (!$new_order) return false;
		
		$j=1;
		foreach ($new_order as $i) {
			$image = Image::findOne($i);
			if ($image) {
				$image->setAttribute('sortOrder', $j);
				$image->save();
				$j++;
				unset($image);
			} else {
				return false;
			}
		}
		return true;
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    //! Сделать
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
