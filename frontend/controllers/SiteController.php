<?php

namespace app\controllers;

use app\models\Category;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\Product;
use rico\yii2images\models\Image;
use Yii;
use yii\data\ArrayDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;

class SiteController extends Controller {
  public function behaviors() {
    return [
      'access' => [
        'class' => AccessControl::className(),
        'only'  => ['logout'],
        'rules' => [
          [
            'actions' => ['logout'],
            'allow'   => true,
            'roles'   => ['@'],
          ],
        ],
      ],
      'verbs'  => [
        'class'   => VerbFilter::className(),
        'actions' => [
          'logout' => ['post'],
        ],
      ],
    ];
  }

  public function actions() {
    return [
      'error'   => [
        'class' => 'yii\web\ErrorAction',
      ],
      'captcha' => [
        'class'           => 'yii\captcha\CaptchaAction',
        'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
      ],
    ];
  }

  public function actionIndex() {
    $gal = new JustifiedGallery(['x' => '1140', 'y' => '640'], ['x' => '0', 'y' => '0']);
    $gal->cut();
    $ratios = $gal->getRatios();
    $count = count($ratios);
    $products = Product::find()->orderBy('RAND()')->limit($count)->all();
    $arr = array();

    foreach ($products as $p) {
      $arr[] = [
        'path' => $p->getImage()->getPath('500x500', ['fit' => true, 'abs' => true]),
        'href' => Url::to(['product/view', 'id' => $p->id]),
      ];
    }

    $gal->fillImages($arr);

    return $this->render('index', [
      'gal' => $gal,
    ]);
  }

  public function actionLogin() {
    if (!\Yii::$app->user->isGuest) {
      return $this->goHome();
    }

    $model = new LoginForm();

    if ($model->load(Yii::$app->request->post()) && $model->login()) {
      return $this->goBack();
    }

    return $this->render('login', [
      'model' => $model,
    ]);
  }

  public function actionLogout() {
    Yii::$app->user->logout();

    return $this->goHome();
  }

  public function actionContact() {
    $model = new ContactForm();

    if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
      Yii::$app->session->setFlash('contactFormSubmitted');

      return $this->refresh();
    }

    return $this->render('contact', [
      'model' => $model,
    ]);
  }

  public function actionAbout() {
    return $this->render('about');
  }

  public function actionFurniture() {
    $categories = (new Category())->find()->asArray()->all();

    foreach ($categories as &$c) {
      $c = ['label' => $c[Yii::t('site', 'description_en')], 'url' => Url::to(['category/view', 'id' => $c['id']])];
    }

    return $this->redirect(['category/view', 'id' => 1]);

    return $this->render('furniture', [
      'categories' => $categories,
      'products'   => new ArrayDataProvider([
        'allModels'  => (new Product())->find()
          ->orderBy(['sort_order' => SORT_ASC])
        //->limit(20)
          ->all(),
        'pagination' => false,
      ]),
    ]);
  }

  public function actionTechnology() {
    return $this->render('technology');
  }
}

class JGSlice {
  public $size;
  public $offset;
  public $finaled = 0;
  public $ratio;
  public $next_cut;
  public function __construct($size, $offset, $next_cut = 'x') {
    $this->size = $size;
    $this->offset = $offset;
    $this->next_cut = $next_cut;
    $this->ratio = ($this->size['y'] > 0) ? $this->size['x'] / $this->size['y'] : 1000;
    return $this;
  }

  public function makeFinal() {
    $this->finaled = 1;
    return $this;
  }
}

class JustifiedGallery {
  public $size;
  public $minheight = 200;
  public $maxheight = 400;
  public $minwidth = 280;
  public $maxwidth = 550;
  public $minratio = 1.1;
  public $maxratio = 1.9;
  public $slices = array();

  public function __construct($size, $offset) {
    $this->size = $size;
    $this->offset = $offset;
    $this->slices[] = new JGSlice($size, $offset);
    return $this;
  }

  function cut() {
    $i = 0;

    while (isset($this->slices[$i]) && $i < 10000) {
      $slice = $this->slices[$i];

      if ($slice->next_cut == 'x') {
        $this->sliceVertical($slice, $i);
      } else {
        $this->sliceHorizontal($slice, $i);
      }

      ++$i;
    }

    return $this;
  }

  function sliceVertical($slice, $i) {
    $size = $slice->size;
    $offset = $slice->offset;

    if ($size['y'] < 2 * $this->minheight) {
      $margin = max([$size['y'] * $this->minratio, $this->minwidth]);
    } else {
      $margin = $this->minwidth;
    }

    if ($size['x'] < 2 * $margin) {
      if ($this->slices[$i]->ratio < $this->minratio) {
        $this->slices[] = new JGSlice($size, $offset, 'y');
        unset($this->slices[$i]);
      } else {
        $this->slices[$i]->makeFinal();
      }

      return $this;
    }

    $cut = rand($margin, $size['x'] - $margin - 1);
    $tmp = $size['x'];
    $size['x'] = $cut;
    $this->slices[] = new JGSlice($size, $offset, 'y');

    $offset['x'] += $size['x'];
    $size['x'] = $tmp - $size['x'];
    $this->slices[] = new JGSlice($size, $offset, 'y');
    unset($this->slices[$i]);
    return $this;
  }

  function sliceHorizontal($slice, $i) {
    $size = $slice->size;
    $offset = $slice->offset;

    if ($size['x'] < 2 * $this->minwidth) {
      $margin = max([$size['x'] / $this->maxratio, $this->minheight]);
    } else {
      $margin = $this->minheight;
    }

    if ($size['y'] < 2 * $margin) {
      if ($this->slices[$i]->ratio > $this->maxratio) {
        $this->slices[] = new JGSlice($size, $offset, 'x');
        unset($this->slices[$i]);
      } else {
        $this->slices[$i]->makeFinal();
      }

      return $this;
    }

    $cut = rand($margin, $size['y'] - $margin - 1);
    $tmp = $size['y'];
    $size['y'] = $cut;
    $this->slices[] = new JGSlice($size, $offset, 'x');

    $offset['y'] += $size['y'];
    $size['y'] = $tmp - $size['y'];
    $this->slices[] = new JGSlice($size, $offset, 'x');
    unset($this->slices[$i]);
    return $this;
  }

  public function getHtml() {
    $html = '<div style="position: relative; width:' . $this->size['x'] . 'px; height:' . $this->size['y'] . 'px;">';

    foreach ($this->slices as $s) {
      $html .= PHP_EOL . '<div style="padding: 5px; position: absolute; width:' . $s->size['x'] . 'px; height:' . $s->size['y'] . 'px; left:' . $s->offset['x'] . 'px; top:' . $s->offset['y'] . 'px;">'
      . '<a style="display: block; background-size:cover; background-position:center; width:100%; height:100%; background-image:url(\'' . ($s->image['path']) . '\')" href="' . $s->image['href'] . '"></a>'

        . '</div>';
    }

    $html .= '</div>';
    return $html;
  }

  public function getRatios() {
    $arr = array();

    foreach ($this->slices as $s) {
      $arr[] = $s->ratio;
    }

    return $arr;
  }

  public function getCount() {
    return count($this->getgetRatios());
  }

  public function fillImages($arr) {
    $i = 0;

    foreach ($this->slices as &$s) {
      $s->image = $arr[$i];
      ++$i;
    }

    return $this;
  }
}
