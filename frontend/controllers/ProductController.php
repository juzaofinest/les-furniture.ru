<?php

namespace app\controllers;

use app\models\Category;
use app\models\Product;
use app\models\ProductSearch;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yz\shoppingcart\ShoppingCart;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller {
//! БИСТАФФ ВКЛЮЧИ ВАЛИДАЦИЮ.
  public $enableCsrfValidation = false;
  public function behaviors() {
    return [
      'verbs'  => [
        'class'   => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'only'  => ['create', 'update', 'uploadimage', 'delete'],
        'rules' => [
          [
            'allow'   => true,
            'actions' => ['login', 'signup'],
            'roles'   => ['?'],
          ],
          [
            'allow'   => true,
            'actions' => ['create', 'update', 'uploadimage', 'delete'],
            'roles'   => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
   * Lists all Product models.
   * @return mixed
   */
  public function actionIndex() {
    $searchModel = new ProductSearch();
    $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

    return $this->render('index', [
      'searchModel'  => $searchModel,
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Product model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    $model = $this->findModel($id);
    $categories = Category::find()
      ->joinWith('products', false)
      ->groupBy([Category::tablename() . '.id'])
      ->having('COUNT(' . Product::tablename() . '.id) > 3')
      ->asArray()
      ->all();

    foreach ($categories as &$c) {
      $c = ['label' => $c[Yii::t('site', 'description_en')], 'url' => ['category/view', 'id' => $c['id']], 'active' => ($model->category_id == $c['id']) ? true : null];
    }

    return $this->render('view', [
      'model'      => $model,
      'categories' => $categories,
    ]);
  }

  /**
   * Creates a new Product model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Product();

    $get = Yii::$app->request->get();

    if (isset($get['category_id'])) {
      $model->load([
        'Product' => ['category_id' => $get['category_id']],
      ]);
    }

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Product model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post())) {
      if ($model->validate()) {
        $model->save(false);

        return $this->redirect(['view', 'id' => $model->id]);
      }
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  public function actionUploadimage($id) {
    $model = $this->findModel($id);
    $file = array_pop($_FILES);

    if (isset($file['tmp_name'])) {
      if (getimagesize($file['tmp_name'])) {
        rename($file['tmp_name'], $file['tmp_name'] . $file['name']);
        $model->attachImage($file['tmp_name'] . $file['name']);
        return true;
      }
    }

    return false;
  }

  /**
   * Deletes an existing Product model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Product model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Product the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Product::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }

  public function actionAddtocart($id) {
    $cart = new ShoppingCart();

    $model = Product::findOne($id);

    if ($model) {
      $cart->put($model, 1);
      return $this->redirect(['view', 'id' => $model->id]);
    }

    throw new NotFoundHttpException();
  }

  public function actionBuyitnow($id) {
    $cart = new ShoppingCart();

    $model = Product::findOne($id);

    if ($model) {
      $cart->put($model, 1);
      return $this->redirect(['cart/view']);
    }

    throw new NotFoundHttpException();
  }
}
