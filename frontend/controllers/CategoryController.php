<?php

namespace app\controllers;

use app\models\Category;
use app\models\Product;
use app\models\ProductFilter;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * CategoryController implements the CRUD actions for Category model.
 */
class CategoryController extends Controller {
  public function behaviors() {
    return [
      'verbs'  => [
        'class'   => VerbFilter::className(),
        'actions' => [
          'delete' => ['post'],
        ],
      ],
      'access' => [
        'class' => AccessControl::className(),
        'only'  => ['create', 'update', 'index', 'delete'],
        'rules' => [
          [
            'allow'   => true,
            'actions' => ['login', 'signup'],
            'roles'   => ['?'],
          ],
          [
            'allow'   => true,
            'actions' => ['create', 'update', 'index', 'delete'],
            'roles'   => ['@'],
          ],
        ],
      ],
    ];
  }

  /**
   * Lists all Category models.
   * @return mixed
   */
  public function actionIndex() {
    $dataProvider = new ActiveDataProvider([
      'query' => Category::find(),
    ]);

    return $this->render('index', [
      'dataProvider' => $dataProvider,
    ]);
  }

  /**
   * Displays a single Category model.
   * @param integer $id
   * @return mixed
   */
  public function actionView($id) {
    $categories = Category::find()
      ->joinWith('products', false)
      ->groupBy([Category::tablename() . '.id'])
      ->having('COUNT(' . Product::tablename() . '.id) > 3')
      ->asArray()
      ->all();

    if (count($categories) == 1 && $id != $categories[0]['id']) {
      return $this->redirect(['category/view', 'id' => $categories[0]['id']]);
    }

    foreach ($categories as &$c) {
      $c = ['label' => $c[Yii::t('site', 'description_en')], 'url' => ['category/view', 'id' => $c['id']]];
    }

    $model = $this->findModel($id);
    $filter = new ProductFilter(['category_id' => $id]);
    $filter->load(Yii::$app->request->get());
    $dataProvider = $filter->search();
    $dataProvider->pagination = false;

    return $this->render('view', [
      'model'      => $model,
      'categories' => $categories,
      'filter'     => $filter,
      'products'   => $dataProvider,
    ]);
  }

  /**
   * Creates a new Category model.
   * If creation is successful, the browser will be redirected to the 'view' page.
   * @return mixed
   */
  public function actionCreate() {
    $model = new Category();

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('create', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Updates an existing Category model.
   * If update is successful, the browser will be redirected to the 'view' page.
   * @param integer $id
   * @return mixed
   */
  public function actionUpdate($id) {
    $model = $this->findModel($id);

    if ($model->load(Yii::$app->request->post()) && $model->save()) {
      return $this->redirect(['view', 'id' => $model->id]);
    } else {
      return $this->render('update', [
        'model' => $model,
      ]);
    }
  }

  /**
   * Deletes an existing Category model.
   * If deletion is successful, the browser will be redirected to the 'index' page.
   * @param integer $id
   * @return mixed
   */
  public function actionDelete($id) {
    $this->findModel($id)->delete();

    return $this->redirect(['index']);
  }

  /**
   * Finds the Category model based on its primary key value.
   * If the model is not found, a 404 HTTP exception will be thrown.
   * @param integer $id
   * @return Category the loaded model
   * @throws NotFoundHttpException if the model cannot be found
   */
  protected function findModel($id) {
    if (($model = Category::findOne($id)) !== null) {
      return $model;
    } else {
      throw new NotFoundHttpException('The requested page does not exist.');
    }
  }
}
