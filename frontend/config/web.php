<?php

$params = require __DIR__ . '/params.php';

$config = [
  'id'             => 'basic',
  'basePath'       => dirname(__DIR__),
  'bootstrap'      => ['languagepicker', 'log'],
  'components'     => [
    'assetManager'   => [
      'appendTimestamp' => true,
    ],
    'request'        => [
      // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
      'cookieValidationKey' => '4g7Ohl53tTFJ7yTt2NiRZu1xdLrIoU06',
    ],
    'cache'          => [
      'class' => 'yii\caching\FileCache',
    ],
    'cart'           => [
      'class'  => 'yz\shoppingcart\ShoppingCart',
      'cartId' => 'my_application_cart',
    ],
    'user'           => [
      'identityClass'   => 'app\models\User',
      'enableAutoLogin' => true,
    ],
    'errorHandler'   => [
      'errorAction' => 'site/error',
    ],
    'i18n'           => [
      'translations' => [
        'app*'  => [
          'class'          => 'yii\i18n\PhpMessageSource',
          'basePath'       => '@yii/messages',
          'sourceLanguage' => 'en',
          'fileMap'        => [
            'app'       => 'yii.php',
            'app/error' => 'error.php',
          ],
        ],
        'yii*'  => [
          'class'          => 'yii\i18n\PhpMessageSource',
          'basePath'       => '@yii/messages',
          'sourceLanguage' => 'en',
          'fileMap'        => [
            'app'       => 'yii.php',
            'app/error' => 'error.php',
          ],
        ],
        'site*' => [
          'class'   => 'yii\i18n\PhpMessageSource',
          //'basePath' => '@app/messages',
          //'sourceLanguage' => 'en-US',
          'fileMap' => [
            'site'       => 'site.php',
            'site/error' => 'error.php',
          ],
        ],
      ],
    ],
    'languagepicker' => [
      'class'      => 'lajax\languagepicker\Component',
      'cookieName' => 'language',
      'expireDays' => 64,
      'languages'  => ['en' => 'EN', 'ru' => 'RU'], // List of available languages (icons only)
    ],
    'mailer'         => [
      'class'            => 'yii\swiftmailer\Mailer',
      // send all mails to a file by default. You have to set
      // 'useFileTransport' to false and configure a transport
      // for the mailer to send real emails.
      'useFileTransport' => false,
    ],
    'log'            => [
      'traceLevel' => YII_DEBUG ? 3 : 0,
      'targets'    => [
        [
          'class'  => 'yii\log\FileTarget',
          'levels' => ['error', 'warning'],
        ],
      ],
    ],
    'db'             => require __DIR__ . '/db.php',
    'urlManager'     => [
      'enablePrettyUrl'     => true,
      'showScriptName'      => false,
      'enableStrictParsing' => false,
      'rules'               => [
        ['class' => 'yii\web\UrlRule', 'pattern' => 'wooden-slab', 'route' => 'category/view', 'defaults' => ['id' => 1]],
        ['class' => 'yii\web\UrlRule', 'pattern' => 'tables', 'route' => 'category/view', 'defaults' => ['id' => 2]],
        ['class' => 'yii\web\UrlRule', 'pattern' => 'accessories', 'route' => 'category/view', 'defaults' => ['id' => 3]],

        //! берет только верхнее правило
        /*
        ['class' => 'yii\web\UrlRule', 'pattern' => 'wooden-slab/<id:\d+>', 'route' => 'product/view'],
        ['class' => 'yii\web\UrlRule', 'pattern' => 'tables/<id:\d+>', 'route' => 'product/view'],
        ['class' => 'yii\web\UrlRule', 'pattern' => 'accessories/<id:\d+>', 'route' => 'product/view'],
         */

        'cart'                      => 'cart/view',
        '<controller:\w+>/<id:\d+>' => '<controller>/view',
        'product/<id:\d+>/update'   => 'product/update',
        'admin'                     => 'site/login',
        'catalog'                   => 'site/furniture',
        'about'                     => 'site/about',
        'contact'                   => 'site/contact',
        'technology'                => 'site/technology',
      ],
    ],
  ],
  'language'       => 'en',
  'modules'        => [
    'yii2images' => [
      'class'           => 'rico\yii2images\Module',
                                                                  //be sure, that permissions ok
                                                                  //if you cant avoid permission errors you have to create "images" folder in web root manually and set 777 permissions
      'imagesStorePath' => 'img/store',                           //path to origin images
      'imagesCachePath' => 'img/cache',                           //path to resized copies
      'graphicsLibrary' => 'GD',                                  //but really its better to use 'Imagick'
      'placeHolderPath' => '@webroot/img/productPlaceholder.png', // if you want to get placeholder when image not exists, string will be processed by Yii::getAlias
    ],
  ],
  'params'         => $params,
  'sourceLanguage' => 'en',
];

if (YII_ENV_DEV) {
  // configuration adjustments for 'dev' environment
  $config['bootstrap'][] = 'debug';
  $config['modules']['debug'] = [
    'class' => 'yii\debug\Module',
  ];

  $config['bootstrap'][] = 'gii';
  $config['modules']['gii'] = [
    'class' => 'yii\gii\Module',
  ];
}

return $config;
