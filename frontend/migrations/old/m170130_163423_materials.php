<?php

use app\models\Material;
use app\models\Product;
use yii\db\Migration;

class m170130_163423_materials extends Migration {
  public function up() {
    $tableOptions = null;

    if ('mysql' === $this->db->driverName) {
      // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    }

    $this->createTable(Material::tableName(), [
      'id'             => $this->primaryKey(),
      'description_ru' => $this->string()->notNull(),
      'description_en' => $this->string()->notNull(),
    ], $tableOptions);

    (new Material([
      'description_en' => 'Oak',
      'description_ru' => 'Дуб',
    ]))->save();

    (new Material([
      'description_en' => 'Cedar',
      'description_ru' => 'Кедр',
    ]))->save();

    (new Material([
      'description_en' => 'Chestnut',
      'description_ru' => 'Каштан',
    ]))->save();

    $this->addColumn(Product::tableName(), 'material_id', $this->integer());
    $this->addForeignKey('fk_product_material', Product::tableName(), 'material_id', Material::tableName(), 'id', 'RESTRICT', 'CASCADE');
    $this->update(Product::tableName(), ['material_id' => 1], ['material_id' => null]);
  }

  public function down() {
    $this->dropTable(Material::tableName());
  }
}
