<?php

use app\models\Cut;
use app\models\Product;
use yii\db\Migration;

class m170131_073534_cut extends Migration {
  public function up() {
    $tableOptions = null;

    if ('mysql' === $this->db->driverName) {
      // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    }

    $this->createTable(Cut::tableName(), [
      'id'             => $this->primaryKey(),
      'description_ru' => $this->string()->notNull(),
      'description_en' => $this->string()->notNull(),
    ], $tableOptions);

    (new Cut([
      'description_en' => 'Transversal',
      'description_ru' => 'Поперечный',
    ]))->save();

    (new Cut([
      'description_en' => 'Lateral',
      'description_ru' => 'Продольный',
    ]))->save();

    $this->addColumn(Product::tableName(), 'cut_id', $this->integer());
    $this->addForeignKey('fk_product_cut', Product::tableName(), 'cut_id', Cut::tableName(), 'id', 'RESTRICT', 'CASCADE');
    $this->update(Product::tableName(), ['cut_id' => 1], ['cut_id' => null]);
  }

  public function down() {
  }
}
