<?php

use app\models\Product;
use yii\db\Migration;

class m170131_074502_moisture extends Migration {
  public function up() {
    $tableOptions = null;

    if ('mysql' === $this->db->driverName) {
      // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    }

    $this->addColumn(Product::tableName(), 'moisture', $this->integer() . ' AFTER description_en');
  }

  public function down() {
  }
}
