<?php

use app\models\Product;
use yii\db\Migration;

class m170130_171110_width extends Migration {
  public function up() {
    $tableOptions = null;

    if ('mysql' === $this->db->driverName) {
      // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    }

    $this->addColumn(Product::tableName(), 'width_ceil', $this->integer() . ' after width');
    $this->addColumn(Product::tableName(), 'width_floor', $this->integer() . ' after width');

    $offset = 0;

    while ($product = Product::find()->offset($offset)->one()) {
      $this->updateProduct($product);
      ++$offset;
    }

    // $this->deleteColumn(Product::tableName(), 'width');
  }

  public function down() {
  }

  protected function updateProduct($product) {
    if (preg_match('/^\d+$/', $product->width, $matches)) {
      $this->update(Product::tableName(), ['width_floor' => $matches[0]], ['id' => $product->id]);
      $this->update(Product::tableName(), ['width_ceil' => $matches[0]], ['id' => $product->id]);
    } elseif (preg_match_all('/^(\d+)-(\d+)$/', $product->width, $matches)) {
      $this->update(Product::tableName(), ['width_floor' => $matches[1][0]], ['id' => $product->id]);
      $this->update(Product::tableName(), ['width_ceil' => $matches[2][0]], ['id' => $product->id]);
    }
  }
}
