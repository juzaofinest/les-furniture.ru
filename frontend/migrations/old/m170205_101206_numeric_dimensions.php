<?php

use app\models\Product;
use yii\db\Migration;

class m170205_101206_numeric_dimensions extends Migration {
  public function up() {
    $tableOptions = null;

    if ('mysql' === $this->db->driverName) {
      // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
      $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
    }

    $this->alterColumn(Product::tableName(), 'length', $this->integer());
    $this->alterColumn(Product::tableName(), 'thickness', $this->integer());
  }

  public function down() {
  }
}
