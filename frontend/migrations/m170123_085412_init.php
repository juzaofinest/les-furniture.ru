<?php

use yii\db\Migration;

class m170123_085412_init extends Migration {
  public function up() {
    $this->execute(file_get_contents(__DIR__ . '/../assets/dump.sql'));
    if (YII_ENV_DEV) {
      $this->delete('{{%image}}', 'id > 0');
    }
  }

  public function down() {
    echo "m161108_084942_new_launch cannot be reverted.\n";

    return true;
  }
}
