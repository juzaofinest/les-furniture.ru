# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: les-furniture.ru (MySQL 5.6.33-79.0)
# Database: u0092485_les
# Generation Time: 2017-03-25 16:03:49 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table les_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `les_categories`;

CREATE TABLE `les_categories` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `description_ru` varchar(32) DEFAULT NULL,
  `description_en` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `les_categories` WRITE;
/*!40000 ALTER TABLE `les_categories` DISABLE KEYS */;

INSERT INTO `les_categories` (`id`, `description_ru`, `description_en`)
VALUES
	(1,'Столешницы','Wooden Slab'),
	(2,'Столы','Tables'),
	(3,'Предметы интерьера','Accessories');

/*!40000 ALTER TABLE `les_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table les_image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `les_image`;

CREATE TABLE `les_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filePath` varchar(400) NOT NULL,
  `itemId` int(20) NOT NULL,
  `isMain` int(1) DEFAULT '0',
  `modelName` varchar(150) NOT NULL,
  `urlAlias` varchar(400) NOT NULL,
  `sortOrder` int(4) NOT NULL DEFAULT '999',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `les_image` WRITE;
/*!40000 ALTER TABLE `les_image` DISABLE KEYS */;

INSERT INTO `les_image` (`id`, `filePath`, `itemId`, `isMain`, `modelName`, `urlAlias`, `sortOrder`)
VALUES
	(1,'Products/Product1/69f5cc.jpg',1,1,'Product','e767f69ef4-1',999),
	(2,'Products/Product1/b656c8.jpg',1,0,'Product','fc65639a53-2',999),
	(3,'Products/Product2/1788b7.jpg',2,1,'Product','7d8f3b412a-1',999),
	(4,'Products/Product2/6eb74e.jpg',2,0,'Product','60c1e32bd7-2',999),
	(5,'Products/Product3/78f127.jpg',3,1,'Product','27b3a82da4-1',999),
	(6,'Products/Product3/f2bfaa.jpg',3,0,'Product','f81960d25e-2',999),
	(7,'Products/Product4/a38baf.jpg',4,1,'Product','9510f29869-1',999),
	(8,'Products/Product4/ff48e0.jpg',4,0,'Product','254dc1bc5c-2',999),
	(9,'Products/Product5/9a97e4.jpg',5,1,'Product','a4fd615568-1',999),
	(10,'Products/Product5/d11fc5.jpg',5,0,'Product','56ba945b6c-2',999),
	(11,'Products/Product6/0639e1.jpg',6,1,'Product','900816bd61-1',999),
	(12,'Products/Product6/d18b5d.jpg',6,0,'Product','c8b5c93356-2',999),
	(13,'Products/Product7/bf2794.jpg',7,1,'Product','675d5f47fe-1',999),
	(14,'Products/Product7/db07e0.jpg',7,0,'Product','492e57bcc0-2',999),
	(15,'Products/Product8/8de241.jpg',8,1,'Product','9492d3d175-1',999),
	(16,'Products/Product8/339a8c.jpg',8,0,'Product','c13e6dd781-2',999),
	(17,'Products/Product9/cbace0.jpg',9,1,'Product','f251325a2a-1',999),
	(18,'Products/Product9/e75b04.jpg',9,0,'Product','0529e7211d-2',999),
	(19,'Products/Product10/f6c208.jpg',10,1,'Product','9a8d5e01cb-1',999),
	(20,'Products/Product10/02cce7.jpg',10,0,'Product','9517ce8347-2',999),
	(21,'Products/Product11/7b8557.jpg',11,1,'Product','5820e0992d-1',999),
	(22,'Products/Product11/fddb1d.jpg',11,0,'Product','c6108529a4-2',999),
	(23,'Products/Product12/987dab.jpg',12,1,'Product','9ae206e85c-1',999),
	(24,'Products/Product12/3151d9.jpg',12,0,'Product','e4eb09f423-2',999),
	(25,'Products/Product13/b224a8.jpg',13,1,'Product','37ef9bcd16-1',999),
	(26,'Products/Product13/7479cf.jpg',13,0,'Product','c9d201f47c-2',999),
	(27,'Products/Product14/e0cab8.jpg',14,1,'Product','24c315bd48-1',999),
	(28,'Products/Product14/a4f811.jpg',14,0,'Product','f829fda5f6-2',999),
	(29,'Products/Product15/c5c839.jpg',15,1,'Product','be3b8bc2f0-1',999),
	(30,'Products/Product15/5ff7cb.jpg',15,0,'Product','9118075416-2',999),
	(31,'Products/Product16/2269af.jpg',16,1,'Product','f28db626e5-1',999),
	(32,'Products/Product16/5e6262.jpg',16,0,'Product','38d8ea7a6b-2',999),
	(33,'Products/Product17/05285c.jpg',17,1,'Product','f353f9fd37-1',999),
	(34,'Products/Product17/1c5545.jpg',17,0,'Product','8e029a0e0e-2',999),
	(35,'Products/Product18/0344cf.jpg',18,1,'Product','7c810de690-1',999),
	(36,'Products/Product18/80cde5.jpg',18,0,'Product','fa39087390-2',999),
	(37,'Products/Product19/f1a406.jpg',19,1,'Product','ff80a2cea5-1',999),
	(38,'Products/Product19/e7c9b6.jpg',19,0,'Product','39ef054785-2',999),
	(39,'Products/Product20/571a71.jpg',20,1,'Product','9835e5faeb-1',999),
	(40,'Products/Product20/5f955a.jpg',20,0,'Product','9c3e884f21-2',999),
	(41,'Products/Product21/89a3f0.jpg',21,1,'Product','a7cfc8b754-1',999),
	(42,'Products/Product21/952309.jpg',21,0,'Product','1eba5b054d-2',999),
	(43,'Products/Product22/96b1ee.jpg',22,1,'Product','58376cee64-1',999),
	(44,'Products/Product22/015b1f.jpg',22,0,'Product','65c7455e2d-2',999),
	(45,'Products/Product23/e8fc3a.jpg',23,1,'Product','d51a390d0e-1',999),
	(46,'Products/Product23/6b7a1a.jpg',23,0,'Product','e1f803f4fb-2',999),
	(47,'Products/Product24/b48a36.jpg',24,1,'Product','67bba6b3b5-1',999),
	(48,'Products/Product24/f98f75.jpg',24,0,'Product','a7c406bae8-2',999),
	(49,'Products/Product25/9d112e.jpg',25,1,'Product','0307081682-1',999),
	(50,'Products/Product25/29b9ba.jpg',25,0,'Product','eef76aac88-2',999),
	(51,'Products/Product26/d270ce.jpg',26,1,'Product','152be91c2a-1',999),
	(52,'Products/Product26/02ffe6.jpg',26,0,'Product','973e2270d6-2',999),
	(53,'Products/Product27/b11fc0.jpg',27,1,'Product','3b8a6f9431-1',999),
	(54,'Products/Product27/a9677a.jpg',27,0,'Product','be8c26c62e-2',999),
	(55,'Products/Product28/dae89f.jpg',28,1,'Product','02510f681b-1',999),
	(56,'Products/Product28/687502.jpg',28,0,'Product','344c29343a-2',999),
	(57,'Products/Product29/6758f1.jpg',29,1,'Product','eca5e72dea-1',999),
	(58,'Products/Product29/04459e.jpg',29,0,'Product','08104c698b-2',999),
	(59,'Products/Product30/ef790d.jpg',30,1,'Product','b205a8180b-1',999),
	(60,'Products/Product30/3ce11c.jpg',30,0,'Product','7f7e1f52c5-2',999),
	(61,'Products/Product31/95e7e6.jpg',31,1,'Product','b2900f55d8-1',999),
	(62,'Products/Product31/d40ebf.jpg',31,0,'Product','f19e014df7-2',999),
	(63,'Products/Product32/c223cb.jpg',32,1,'Product','c4007246ee-1',999),
	(64,'Products/Product32/b83cfb.jpg',32,0,'Product','2384655c9e-2',999),
	(65,'Products/Product33/b56fff.jpg',33,1,'Product','4ae49daf44-1',999),
	(66,'Products/Product33/6f6774.jpg',33,0,'Product','ff27c149ff-2',999),
	(67,'Products/Product34/4625c0.jpg',34,1,'Product','3eb7b71f88-1',999),
	(68,'Products/Product34/381b26.jpg',34,0,'Product','a5daf7fa61-2',999),
	(69,'Products/Product35/69ee6b.jpg',35,1,'Product','03a1afb4ea-1',999),
	(70,'Products/Product35/0a3305.jpg',35,0,'Product','b121759853-2',999),
	(71,'Products/Product36/a6ba65.jpg',36,1,'Product','a1ba6a24af-1',999),
	(72,'Products/Product36/9d864f.jpg',36,0,'Product','069d1cc43a-2',999),
	(73,'Products/Product37/fcdd51.jpg',37,1,'Product','54f1faace9-1',999),
	(74,'Products/Product37/18b41a.jpg',37,0,'Product','ba0a363814-2',999),
	(75,'Products/Product38/f8ff6e.jpg',38,1,'Product','4e5b65ecd4-1',999),
	(76,'Products/Product38/8337fb.jpg',38,0,'Product','190ec98b72-2',999),
	(77,'Products/Product39/d5d6b1.jpg',39,1,'Product','7450d9ca21-1',999),
	(78,'Products/Product39/e9c836.jpg',39,0,'Product','871ae78d15-2',999),
	(79,'Products/Product40/a3bbc7.jpg',40,1,'Product','8137c0c6fa-1',999),
	(80,'Products/Product40/7d9ef9.jpg',40,0,'Product','bf71bb3896-2',999),
	(81,'Products/Product41/4b7937.jpg',41,1,'Product','a2546dcd09-1',999),
	(82,'Products/Product41/e47cf4.jpg',41,0,'Product','fc9afb3847-2',999),
	(83,'Products/Product42/a0bdb9.jpg',42,1,'Product','bbc7b49ea0-1',999),
	(84,'Products/Product42/e7293f.jpg',42,0,'Product','550c3ef8bb-2',999),
	(85,'Products/Product42/a9468c.jpg',42,0,'Product','4576fc7d39-3',999),
	(86,'Products/Product43/873801.jpg',43,1,'Product','8d115be3d6-1',999),
	(87,'Products/Product43/cc81b8.jpg',43,0,'Product','be41b0b827-2',999),
	(88,'Products/Product44/214ebf.jpg',44,1,'Product','0501bd35ed-1',999),
	(89,'Products/Product44/4e16fe.jpg',44,0,'Product','425ed55df8-2',999),
	(90,'Products/Product45/3c5622.jpg',45,1,'Product','abf17e4f69-1',999),
	(91,'Products/Product45/27e80d.jpg',45,0,'Product','0ced6d91a1-2',999),
	(92,'Products/Product46/35258b.jpg',46,1,'Product','77aab568ef-1',999),
	(93,'Products/Product46/39fbe7.jpg',46,0,'Product','296b51303b-2',999),
	(94,'Products/Product47/97b62f.jpg',47,1,'Product','c9a9df1e36-1',999),
	(95,'Products/Product47/5b849f.jpg',47,0,'Product','9a23e2400c-2',999),
	(96,'Products/Product48/9f9976.jpg',48,1,'Product','3254f74257-1',999),
	(97,'Products/Product48/c61ea3.jpg',48,0,'Product','0ec0df6e20-2',999),
	(98,'Products/Product49/cee829.jpg',49,1,'Product','5f8fffc705-1',999),
	(99,'Products/Product49/61c662.jpg',49,0,'Product','f2c6147e76-2',999),
	(100,'Products/Product50/e43c1d.jpg',50,1,'Product','dad0dd5fe0-1',999),
	(101,'Products/Product50/18f5bc.jpg',50,0,'Product','c8e2aff869-2',999),
	(102,'Products/Product51/80a4b8.jpg',51,1,'Product','fde77f2477-1',999),
	(103,'Products/Product51/e3a388.jpg',51,0,'Product','71281c33af-2',999),
	(104,'Products/Product52/11dc21.jpg',52,1,'Product','6d984ae8ab-1',999),
	(105,'Products/Product52/284937.jpg',52,0,'Product','acca7a5b47-2',999),
	(106,'Products/Product53/74eadd.jpg',53,1,'Product','93dd39011c-1',999),
	(107,'Products/Product53/6d62eb.jpg',53,0,'Product','cff1896b47-2',999),
	(108,'Products/Product54/ad9d92.jpg',54,1,'Product','23b7dcd0e1-1',999),
	(109,'Products/Product54/eed21c.jpg',54,0,'Product','8cc1ac9bbe-2',999),
	(110,'Products/Product55/fd22ab.jpg',55,1,'Product','8e788be9c3-1',999),
	(111,'Products/Product55/c1be5f.jpg',55,0,'Product','bccba76f0c-2',999),
	(112,'Products/Product56/b072a7.jpg',56,1,'Product','92cdd42c5c-1',999),
	(113,'Products/Product56/35612d.jpg',56,0,'Product','3c34ba092b-2',999),
	(114,'Products/Product57/2b4180.jpg',57,1,'Product','d8274315d8-1',999),
	(115,'Products/Product57/68f5bb.jpg',57,0,'Product','39d34c777e-2',999),
	(116,'Products/Product58/a5f148.jpg',58,1,'Product','055511dcf1-1',999),
	(117,'Products/Product58/5dc674.jpg',58,0,'Product','4cf3bd1db7-2',999),
	(118,'Products/Product59/041fd8.jpg',59,1,'Product','98679b2ef5-1',999),
	(119,'Products/Product59/177e25.jpg',59,0,'Product','cb564b294b-2',999),
	(120,'Products/Product60/f8ef5f.jpg',60,1,'Product','0a23c06940-1',999),
	(121,'Products/Product60/05706c.jpg',60,0,'Product','61474938c5-2',999),
	(122,'Products/Product61/acf4c6.jpg',61,1,'Product','39ade76703-1',999),
	(123,'Products/Product61/0aba2f.jpg',61,0,'Product','dbbb8b90a0-2',999),
	(124,'Products/Product62/728de1.jpg',62,1,'Product','4b23b6ddb1-1',999),
	(125,'Products/Product62/1474ed.jpg',62,0,'Product','6c7f76c676-2',999),
	(126,'Products/Product63/7dabd1.jpg',63,1,'Product','f7303edb1c-1',999),
	(127,'Products/Product63/53f7c6.jpg',63,0,'Product','1b898100df-2',999),
	(128,'Products/Product64/310651.jpg',64,1,'Product','7d6af6ec74-1',999),
	(129,'Products/Product64/ab0439.jpg',64,0,'Product','f85e80d5db-2',999),
	(130,'Products/Product65/4da037.jpg',65,1,'Product','30d4a26860-1',999),
	(131,'Products/Product65/b71658.jpg',65,0,'Product','74f3c19f2e-2',999),
	(132,'Products/Product66/f2b1fb.jpg',66,1,'Product','c023127952-1',999),
	(133,'Products/Product66/92affb.jpg',66,0,'Product','6ba8d5dd52-2',999),
	(134,'Products/Product67/b457fb.jpg',67,1,'Product','011a23df28-1',999),
	(135,'Products/Product67/2046b7.jpg',67,0,'Product','fd87e1c2db-2',999),
	(136,'Products/Product68/e36fc7.jpg',68,1,'Product','1c8b721ae2-1',999),
	(137,'Products/Product68/aa7326.jpg',68,0,'Product','9e9c57d8c5-2',999),
	(138,'Products/Product69/8f295a.jpg',69,1,'Product','bd205078ff-1',999),
	(139,'Products/Product69/0f757b.jpg',69,0,'Product','397de5327b-2',999);

/*!40000 ALTER TABLE `les_image` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table les_migration
# ------------------------------------------------------------

DROP TABLE IF EXISTS `les_migration`;

CREATE TABLE `les_migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `les_migration` WRITE;
/*!40000 ALTER TABLE `les_migration` DISABLE KEYS */;

INSERT INTO `les_migration` (`version`, `apply_time`)
VALUES
	('m000000_000000_base',1439746855);

/*!40000 ALTER TABLE `les_migration` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table les_order
# ------------------------------------------------------------

DROP TABLE IF EXISTS `les_order`;

CREATE TABLE `les_order` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(64) NOT NULL,
  `phone` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL DEFAULT '',
  `payment_method` varchar(64) NOT NULL,
  `cart` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table les_products
# ------------------------------------------------------------

DROP TABLE IF EXISTS `les_products`;

CREATE TABLE `les_products` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `sku_ru` varchar(64) NOT NULL DEFAULT '0',
  `sku_en` varchar(255) DEFAULT NULL,
  `model_ru` varchar(64) NOT NULL DEFAULT '',
  `model_en` varchar(64) NOT NULL DEFAULT '',
  `moisture` int(11) DEFAULT NULL,
  `category_id` int(11) unsigned DEFAULT NULL,
  `price` int(11) unsigned NOT NULL DEFAULT '0',
  `quantity` int(4) unsigned NOT NULL DEFAULT '0',
  `length` int(11) DEFAULT NULL,
  `width_floor` int(11) DEFAULT NULL,
  `width_ceil` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `thickness` int(11) DEFAULT NULL,
  `sort_order` int(4) unsigned DEFAULT '0',
  `material_id` int(11) DEFAULT NULL,
  `cut_id` int(11) DEFAULT NULL,
  `status_id` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`),
  KEY `fk_product_material` (`material_id`),
  KEY `fk_product_cut` (`cut_id`),
  CONSTRAINT `les_products_ibfk_3` FOREIGN KEY (`category_id`) REFERENCES `les_categories` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `les_products` WRITE;
/*!40000 ALTER TABLE `les_products` DISABLE KEYS */;

INSERT INTO `les_products` (`id`, `sku_ru`, `sku_en`, `model_ru`, `model_en`, `moisture`, `category_id`, `price`, `quantity`, `length`, `width_floor`, `width_ceil`, `height`, `thickness`, `sort_order`, `material_id`, `cut_id`, `status_id`)
VALUES
	(1,'А1','A1','Необработанный спил','Solid wooden slab',9,1,22700,1,'1500',600,600,'0','100',NULL,1,2,1),
	(2,'А2','A2','Необработанный спил','Solid wooden slab',9,1,24600,1,'1500',650,650,'0','100',NULL,1,2,1),
	(3,'А3','A3','Необработанный спил','Solid wooden slab',9,1,20700,1,'1300',630,630,'0','100',NULL,1,2,1),
	(4,'Б1','B1','Необработанный спил','Solid wooden slab',9,1,18100,1,'2940',415,445,'0','85',NULL,10,1,1),
	(6,'К3','K3','Необработанный спил','Solid wooden slab',9,1,15800,1,'1950',300,500,'0','80',NULL,2,1,1),
	(7,'К4','K4','Необработанный спил','Solid wooden slab',9,1,20200,1,'1800',440,620,'0','80',NULL,2,1,1),
	(8,'К5','K5','Необработанный спил','Solid wooden slab',9,1,25100,1,'2180',340,800,'0','80',NULL,2,1,1),
	(9,'КА2','KA2','Необработанный спил','Solid wooden slab',9,1,21500,1,'1000',850,850,'0','100',NULL,3,2,1),
	(10,'КА61','KA6','Необработанный спил','Solid wooden slab',9,1,49700,1,'4400',350,770,'0','80',NULL,3,1,1),
	(11,'КА8','KA8','Необработанный спил','Solid wooden slab',9,1,43800,1,'2400',790,1020,'0','80',NULL,3,1,1),
	(13,'КА12','KA12','Необработанный спил','Solid wooden slab',9,1,32800,1,'2750',520,660,'0','80',NULL,3,1,1),
	(14,'КА15','KA15','Необработанный спил','Solid wooden slab',9,1,30500,1,'2400',500,760,'0','80',NULL,3,1,1),
	(15,'КА16','KA16','Необработанный спил','Solid wooden slab',9,1,42400,1,'2800',530,970,'0','80',NULL,3,1,1),
	(16,'КА17','KA17','Необработанный спил','Solid wooden slab',9,1,33700,1,'2400',550,840,'0','80',NULL,3,1,1),
	(17,'КА20','KA20','Необработанный спил','Solid wooden slab',9,1,36200,1,'3170',480,650,'0','80',NULL,3,1,1),
	(18,'КА21','KA21','Необработанный спил','Solid wooden slab',9,1,35100,1,'2460',640,775,'0','80',NULL,3,1,1),
	(19,'КА22','KA22','Необработанный спил','Solid wooden slab',9,1,59500,1,'4500',500,810,'0','80',NULL,3,1,1),
	(20,'КА23','KA23','Необработанный спил','Solid wooden slab',9,1,54600,1,'4100',620,700,'0','80',NULL,3,1,1),
	(21,'КА25','KA25','Необработанный спил','Solid wooden slab',9,1,46600,1,'4140',515,600,'0','80',NULL,3,1,1),
	(22,'КА26','KA26','Необработанный спил','Solid wooden slab',9,1,50300,1,'4450',410,710,'0','80',NULL,3,1,1),
	(23,'КА29','KA29','Необработанный спил','Solid wooden slab',9,1,27300,1,'2850',400,550,'0','80',NULL,3,1,1),
	(25,'КА31','KA31','Необработанный спил','Solid wooden slab',9,1,28100,1,'1930',640,800,'0','80',NULL,3,1,1),
	(26,'КА33','KA33','Необработанный спил','Solid wooden slab',9,1,18000,1,'1400',535,740,'0','80',NULL,3,1,1),
	(27,'КА35','KA35','Необработанный спил','Solid wooden slab',9,1,16100,1,'1970',380,430,'0','80',NULL,3,1,1),
	(28,'КА40','KA40','Необработанный спил','Solid wooden slab',9,1,15200,1,'1112',550,800,'0','80',NULL,3,1,1),
	(29,'КА42','KA42','Необработанный спил','Solid wooden slab',9,1,16400,1,'1112',640,820,'0','80',NULL,3,1,1),
	(30,'КА50','KA50','Необработанный спил','Solid wooden slab',9,1,39300,1,'3300',550,630,'0','80',NULL,3,1,1),
	(31,'КА51','KA51','Необработанный спил','Solid wooden slab',9,1,35100,1,'2500',570,700,'0','80',NULL,3,1,1),
	(32,'КА52','KA52','Необработанный спил','Solid wooden slab',9,1,46200,1,'3400',580,650,'0','80',NULL,3,1,1),
	(33,'КА53','KA53','Необработанный спил','Solid wooden slab',9,1,54600,1,'3450',700,870,'0','80',NULL,3,1,1),
	(34,'КА54','KA54','Необработанный спил','Solid wooden slab',9,1,35900,1,'2500',600,700,'0','80',NULL,3,1,1),
	(35,'КА55','KA55','Необработанный спил','Solid wooden slab',9,1,29900,1,'2480',490,600,'0','80',NULL,3,1,1),
	(36,'КА56','KA56','Необработанный спил','Solid wooden slab',9,1,48800,1,'3380',610,820,'0','80',NULL,3,1,1),
	(37,'КА57','KA57','Необработанный спил','Solid wooden slab',9,1,39200,1,'3380',480,670,'0','80',NULL,3,1,1),
	(39,'КА59','KA59','Необработанный спил','Solid wooden slab',9,1,37800,1,'2440',650,750,'0','80',NULL,3,1,1),
	(40,'КА60','KA60','Необработанный спил','Solid wooden slab',9,1,30900,1,'3400',380,520,'0','80',NULL,3,1,1),
	(41,'КШ2','KS2','Обработанный спил','Solid wooden slab',9,1,45400,1,'2900',650,730,'0','90',NULL,4,1,1),
	(42,'О20','O20','Необработанный спил','Solid wooden slab',9,1,28400,1,'1600',650,650,'0','65',NULL,5,1,1),
	(43,'Г2','G2','Необработанный спил','Solid wooden slab',9,1,4000,1,'1060',250,310,'0','40',NULL,6,1,1),
	(44,'Б2','B2','Необработанный спил','Solid wooden slab',9,1,10100,1,'2680',300,360,'0','90',NULL,10,1,1),
	(45,'Д1','D1','Необработанный спил','Solid wooden slab',9,1,9200,1,'1300',490,630,'0','60',NULL,7,1,1),
	(47,'Д3','D3','Необработанный спил','Solid wooden slab',9,1,8400,1,'720',540,540,'0','50',NULL,7,2,1),
	(49,'Д5','D5','Необработанный спил','Solid wooden slab',9,1,8400,1,'730',570,570,'0','50',NULL,7,2,1),
	(50,'КД1','KD1','Необработанный спил','Solid wooden slab',9,1,16100,1,'2070',560,610,'0','70',NULL,9,1,1),
	(51,'КД2','KD2','Необработанный спил','Solid wooden slab',9,1,19700,1,'2010',680,800,'0','70',NULL,9,1,1),
	(52,'КД3','KD3','Необработанный спил','Solid wooden slab',9,1,36600,1,'3900',540,700,'0','80',NULL,9,1,1),
	(53,'КД4','KD4','Необработанный спил','Solid wooden slab',9,1,49300,1,'3900',770,900,'0','80',NULL,9,1,1),
	(54,'КД5','KD5','Необработанный спил','Solid wooden slab',9,1,42100,1,'4060',630,740,'0','80',NULL,9,1,1),
	(55,'КД6','KD6','Необработанный спил','Solid wooden slab',9,1,41200,1,'4000',650,710,'0','80',NULL,9,1,1),
	(56,'КШ3','KS3','Необработанный спил','Solid wooden slab',9,1,2000,1,'820',550,550,'0','100',NULL,4,1,1),
	(57,'КШ4','KS4','Необработанный спил','Solid wooden slab',9,1,2000,1,'1165',335,335,'0','90',NULL,4,1,1),
	(58,'КШ5','KS5','Необработанный спил','Solid wooden slab',9,1,2000,1,'700',650,650,'0','90',NULL,4,1,1),
	(59,'КШ6','KS6','Необработанный спил','Solid wooden slab',9,1,13100,1,'925',840,840,'0','80',NULL,4,1,1),
	(60,'КШ7','KS7','Необработанный спил','Solid wooden slab',9,1,19600,1,'1290',720,880,'0','90',NULL,4,1,1),
	(61,'КШ8','KS8','Необработанный спил','Solid wooden slab',9,1,21000,1,'2900',360,500,'0','80',NULL,4,1,1),
	(62,'КШ9','KS9','Необработанный спил','Solid wooden slab',9,1,61400,1,'4430',400,920,'0','100',NULL,4,1,1),
	(64,'МД1','MD1','Необработанный спил','Solid wooden slab',9,1,15200,1,'2900',300,320,'0','40',NULL,8,1,1),
	(65,'МД2','MD2','Необработанный спил','Solid wooden slab',9,1,20300,1,'2800',420,440,'0','40',NULL,8,1,1),
	(67,'О21','O21','Необработанный спил','Solid wooden slab',9,1,10500,1,'795',670,670,'0','55',NULL,5,1,1),
	(68,'Т1','T1','Необработанный спил','Solid wooden slab',9,1,12600,1,'1400',340,420,'0','50',NULL,11,1,1),
	(69,'Я1','Y1','Необработанный спил','Solid wooden slab',9,1,9100,1,'2580',500,540,'0','40',NULL,12,1,1);

/*!40000 ALTER TABLE `les_products` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
