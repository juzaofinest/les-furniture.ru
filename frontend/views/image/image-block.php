<?php

use yii\helpers\Html;
use yii\helpers\Url;

$img_id = $p_img->id;
?>

<div id="image-block-<?= $img_id; ?>" class="<?= ($p_img->isMain) ? 'main-image ' : ''; ?>product-image dropdown" style="float: left;">
	<img src="<?= $p_img->getPath('150x150', ['abs' => true]); ?>" data-image-id="<?= $img_id; ?>" data-ismain="<?= $p_img->isMain; ?>" data-toggle="dropdown"/>	
	<ul class="dropdown-menu">
		<li class="set-main-image"><a onclick="setMainImage(<?= $img_id; ?>)">Set as main</a></li>
		<li class="delete-image"><a onclick="deleteImage(<?= $img_id; ?>);">Delete</a></li>
	</ul>
</div>
