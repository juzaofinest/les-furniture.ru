<?php

  /* @var $this yii\web\View */
  /* @var $form yii\bootstrap\ActiveForm */
  /* @var $model app\models\ContactForm */

  use yii\bootstrap\ActiveForm;
  use yii\helpers\Html;

  $this->title = Yii::t('site', 'Contacts');
  $this->params['breadcrumbs'][] = $this->title;
  $address = [
    'ru' => 'Московская область<br/>
Красногорский район<br/>
п/о Петрово-Дальнеe<br/>
На территории АО «Биомед» им. И.И. Мечникова',
    'en' => 'Mechnikovo<br/>
Krasnogorskiy rayon<br/>
Petrovo-Dalnee<br/>
Zavod im. I.I. Mechnikova',
  ];

?>
<div class="site-contact">
    <?php
    if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

        <div class="alert alert-success">
            <?=Yii::t('app', 'Thank you for contacting us. We will respond to you as soon as possible.')?>
        </div>

    <?php endif;?>

    <div class="col-xs-5">
	    <h4 class="letter-spacing-meduim text-uppercase"><?=Yii::t('site', 'Form')?></h4>
        <?php $form = ActiveForm::begin(['id' => 'contact-form']);?>

            <?=$form->field($model, 'name', ['template' => '{input}'])->textInput(['placeholder' => Yii::t('site', 'Name')]);?>

            <?=$form->field($model, 'email', ['template' => '{input}'])->textInput(['placeholder' => Yii::t('site', 'E-mail')]);?>

<!--                     <?=$form->field($model, 'subject')?> -->

            <?=$form->field($model, 'body', ['template' => '{input}'])->textArea(['rows' => 6, 'placeholder' => Yii::t('site', 'Your Inquiry')])?>

            <div class="form-group">
                <?=Html::submitButton(Yii::t('site', 'Send'), ['class' => 'les-button pull-right', 'name' => 'contact-button'])?>
            </div>

        <?php ActiveForm::end();?>

    </div>

    <div class="col-xs-6 col-xs-offset-1">
	    <h2 class="text-uppercase letter-spacing-meduim"><?=Html::encode($this->title)?></h2>
		<h4 class="text-uppercase"><?=Yii::t('site', 'Showroom')?></h4>
		<p class="text-grey">
			<?=$address[Yii::$app->language]?>
		</p>
		<h4 class="text-uppercase"><?=Yii::t('site', 'Phone')?></h4>
		<p class="text-grey">
			+7 905 709 9571
		</p>
		<h4 class="text-uppercase"><?=Yii::t('site', 'E-mail')?></h4>
		<p class="text-grey">
			info@les-furniture.ru
		</p>
		<script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=KgPJ64FXXo6L_56eWBoSSNcFvBvVOuN-&amp;width=540&amp;height=400&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
    </div>

</div>
