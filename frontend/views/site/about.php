<?php

  /* @var $this yii\web\View */

  use yii\helpers\Html;
  use yii\helpers\Url;

  $this->title = Yii::t('site', 'About');
  $this->params['breadcrumbs'][] = $this->title;
  $catalog_url = '<a style="text-decoration:none; color:#333;" href="' . Url::to(['site/furniture']) . '">' . Yii::t('site', 'Catalog') . '</a>';
  $contact_url = '<a style="text-decoration:none; color:#333;" href="' . Url::to(['site/contact']) . '">' . Yii::t('site', 'showroom') . '</a>';

  $about = [
    'ru' => 'Компания LES специализируется на поставке и обработке массива дерева для создания современных и экологичных интерьерных решений. Мы не просто продаем мебель, мы обрабатываем вековой материал для того, чтобы он качественно служил и дарил эстетическое удовольствие не одному поколению владельцев. У нас нет серийных продуктов - каждый предмет уникален, это определяет наш стиль работы - мы стремимся предложить индивидуальный подход каждому клиенту. В нашей мастерской представлен широкий выбор спилов из различных пород дерева, а также, изготовленные из них, предметы интерьера. ' . $catalog_url . ' на нашем сайте всегда отражает актуальный ассортимент. Получить качественную консультацию и задать все интересующие вас вопросы вы можете посетив нашу <a style="text-decoration:none; color:#333;" href="' . Url::to(['site/contact']) . '">мастерскую</a>, а также по телефону или <a style="text-decoration:underline; color:#777;" href="mailto:info@les-furniture.ru">электронной почте</a>.',
    'en' => 'LES Company specializes in supplying and processing of finest domestic wood available to create a modern and eco-friendly interior solutions. We do not just sell furniture, we treat the age-old material to ensure that it will serve and give aesthetic pleasure to many generation of owners. We do not have mass products - every item is unique, it defines our way of working - we strive to offer an individual approach for each customer. Our workshop offers a wide range of slabs of different types of wood, and interior items made out of this wood. Website ' . $catalog_url . ' always reflects our actual range of products. You can always get quality advice and ask all your questions while visiting our <a style="text-decoration:none; color:#333;" href="' . Url::to(['site/contact']) . '">workshop</a> as well as by phone or <a style="text-decoration:underline; color:#777;" href="mailto:info@les-furniture.ru">e-mail</a>',
  ];

?>
<div class="row site-about">

    <div class="col-xs-6 space-top-medium">
      <div class="instafeed text-center" style="overflow:hidden;" data-user-name="les_furniture">
<!-- LightWidget WIDGET --><script src="//lightwidget.com/widgets/lightwidget.js"></script><iframe src="//lightwidget.com/widgets/4fa892e260565ba5834592f52a03bed8.html" scrolling="no" allowtransparency="true" class="lightwidget-widget" style="width: 100%; border: 0; overflow: hidden;"></iframe>
      </div>
    </div>

    <div class="col-xs-6">
      <h2 class="text-uppercase letter-spacing-meduim"><?=Html::encode($this->title)?></h1>
      <p class="text-grey">
      <?=$about[Yii::$app->language]?>
      </p>
    </div>

</div>
