<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

$this->title = Yii::t('site', 'Catalog');
$this->params['breadcrumbs'][] = $this->title;
$this->params['secondary_nav_items'] = $categories;
?>
<div class="furniture-categories">
	<?= ListView::widget([
		'dataProvider' => $products,
		'layout' => '{items}',
		'itemView' => '/product/category_preview',
		'itemOptions' => ['tag' => false],
		'options' => ['class' => 'clearfix'],
	]); ?>
</div>
