<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = Yii::t('site', 'Technology');
$this->params['breadcrumbs'][] = $this->title;
$faker = Faker\Factory::create();
?>
<div class="site-about">
    <h1 class="text-center"><?= Html::encode($this->title) ?></h1>

    <div class="col-xs-8 col-xs-offset-2">
	    <p>
		    Наши высокие технологии.
	    </p>
        <p><?=$faker->text ?></p>
    </div>

</div>
