<?php

use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$product_images = $model->getImages();
$modelId = $model->id;
$uploadimageurl = Url::to(['product/uploadimage', 'id' => $model->id]);

$deleteimageajaxurl = Url::to(['image/deleteajax']);
$setmainimageurl = Url::to(['image/setmain']);
$singleimageurl = Url::to(['image/lastimage']);
$imagesorturl = Url::to(['image/sort']);

$js = <<<JS
var dropZone = $('#dropZone'),
    maxFileSize = 5000000; // максимальный размер фалйа - 1 мб.

// Проверка поддержки браузером
if (typeof(window.FileReader) == 'undefined') {
    dropZone.text('Не поддерживается браузером!');
    dropZone.addClass('error');
}

// Добавляем класс hover при наведении
dropZone[0].ondragover = function() {
    dropZone.addClass('hover');
    return false;
};

// Убираем класс hover
dropZone[0].ondragleave = function() {
    dropZone.removeClass('hover');
    return false;
};

// Обрабатываем событие Drop
dropZone[0].ondrop = function(event) {
    event.preventDefault();
    dropZone.removeClass('hover');
    dropZone.addClass('drop');
    
    var file;
    for (i = 0; i < event.dataTransfer.files.length; i++) {
		file = event.dataTransfer.files[i];
		
	    // Проверяем размер файла
	    if (file.size > maxFileSize) {
	        dropZone.text('Файл слишком большой!');
	        dropZone.addClass('error');
	        return false;
	    }
	    if(!file.type.match('image')){
	        dropZone.text('Не картинка!');
	        dropZone.addClass('error');
	        return false;
	    }
	    
	    // Создаем запрос
	    var formData = new FormData();
		formData.append(file.name, file);
	    var xhr = new XMLHttpRequest();
	    xhr.upload.addEventListener('progress', uploadProgress, false);
	    xhr.onreadystatechange = stateChange;
	    xhr.open('POST', '{$uploadimageurl}', true);
	    xhr.send(formData);
	}
};

// Показываем процент загрузки
function uploadProgress(event) {
    var percent = parseInt(event.loaded / event.total * 100);
    dropZone.text('Загрузка: ' + percent + '%');
}

// Пост обрабочик
function stateChange(event) {
    if (event.target.readyState == 4) {
        if (event.target.status == 200) {
	        if (event.target.response == true) {
	            dropZone.text('Загрузка успешно завершена!');
				$.ajax({
					method: 'POST',
					url: '$singleimageurl',
					data: {
						modelId: $modelId,
					},
					//! типа надо сделать, чтобы аплопдер возвращал айдишник загруженной фотки и запрашивать фотку по айдишнику, а то визникает ошибка, когда много фоток грузишь
					success: function(response) {
						if(response) {
							$('#dropZone').before(response);
							$('.product-update .product-images').sortable('refresh');
						}
					}
				});
	        } else {
	            dropZone.text('Это не картинка?!');
	            dropZone.addClass('error');
	        }
        } else {
            dropZone.text('Произошла ошибка!');
            dropZone.addClass('error');
        }
    }
}

$('.product-update .product-images').sortable({
	items: ".product-image:not(.main-image, #dropZone)",
	opacity: 0.5,
	revert: 100,
	containment: ".product-images",
	placeholder: 'ui-state-highlight',
	tolerance: "pointer",
	stop: function( event, ui ) {
		var res = $('.product-update .product-images').sortable("serialize", {key: "data-image-id"});
		res = res.replace(/data-image-id=/g,'');

		$.ajax({
			method: 'POST',
			url: '$imagesorturl',
			data: {
				new_order: res,
			},
			success: function(response) {
				if (response == true) {
					$('.product-update .product-images').sortable
				}
			}
		});
	}
});
JS;
$this->registerJs($js);

$js= <<<JS
function deleteImage(id) {
	if (confirm('Удалить?')) {
		$.ajax({
			method: 'POST',
			url: '$deleteimageajaxurl',
			data: {
				id: id,
			},
			success: function(response) {
				if(response == true) {
					$('#image-block-' + id).remove();
				}
			}
		});
	}
}
function setMainImage(id) {
	$.ajax({
		method: 'POST',
		url: '$setmainimageurl',
		data: {
			id: id,
		},
		success: function(response) {
			if(response == true) {
				$('.main-image.product-image').removeClass('main-image');
				$('#image-block-' + id).addClass('main-image');
				$('.product-update .product-images').prepend($('.product-update .product-images .main-image'));
				$('.product-update .product-images').sortable('refresh');
			}
		}
	});
}
JS;
$this->registerJs($js, yii\web\View::POS_END, null);

?>

<?php if ($product_images) foreach ($product_images as $p_img) {
	echo $this->render('/image/image-block', [ 'p_img' => $p_img ]);
} ?>
<div id="dropZone" class="product-image text-center" style="">
	СЮДА
</div>
