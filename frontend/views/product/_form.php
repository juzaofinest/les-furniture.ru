<?php

  use app\models\Category;
  use app\models\Cut;
  use app\models\Material;
  use app\models\Status;
  use yii\helpers\ArrayHelper;
  use yii\helpers\Html;
  use yii\widgets\ActiveForm;

  /* @var $this yii\web\View */
  /* @var $model app\models\Product */
  /* @var $form yii\widgets\ActiveForm */

?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>

    <?=$form->field($model, 'sku_ru')->textInput()?>
    <?=$form->field($model, 'sku_en')->textInput()?>

    <?=$form->field($model, 'status_id')->dropDownList(
  ArrayHelper::map(Status::all(), 'id', Yii::t('site', 'description_en')),
  [])
?>

    <?=$form->field($model, 'model_ru')->textInput(['maxlength' => true])?>
    <?=$form->field($model, 'model_en')->textInput(['maxlength' => true])?>

    <?=$form->field($model, 'material_id')->dropDownList(
  ArrayHelper::map(Material::all(), 'id', Yii::t('site', 'description_en')),
  [])
?>

    <?=$form->field($model, 'cut_id')->dropDownList(
  ArrayHelper::map(Cut::all(), 'id', Yii::t('site', 'description_en')),
  [])
?>

    <?=$form->field($model, 'category_id')->dropDownList(
  ArrayHelper::map(Category::find()->asArray()->all(), 'id', Yii::t('site', 'description_en')),
  []
)?>

    <?=$form->field($model, 'price')->textInput()?>

    <?=$form->field($model, 'quantity')->textInput()?>

    <?=$form->field($model, 'length')->textInput()?>

    <?=$form->field($model, 'width_floor')->textInput()?>
    <?=$form->field($model, 'width_ceil')->textInput()?>

    <?=$form->field($model, 'height')->textInput()?>

    <?=$form->field($model, 'thickness')->textInput()?>


    <?=$form->field($model, 'sort_order')->textInput()?>

    <div class="form-group">
        <?=Html::submitButton($model->isNewRecord ? Yii::t('app', 'Create') : Yii::t('app', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary'])?>
    </div>

    <?php ActiveForm::end();?>

</div>
