<?php

  use app\widgets\ProductDescription;
  use app\widgets\ProductDimensions;
  use yii\helpers\Html;
  use yii\helpers\Url;
  use yii\widgets\Breadcrumbs;
  use yii\widgets\DetailView;

  /* @var $this yii\web\View */
  /* @var $model app\models\Product */

  $this->title = $model->{Yii::t('site', 'model_en')};
  $category = $model->getCategory()->one();
  $this->params['breadcrumbs'][] = ['label' => Yii::t('site', 'Catalog'), 'url' => ['site/furniture']];
  $this->params['breadcrumbs'][] = ['label' => $category->{Yii::t('site', 'description_en')}, 'url' => ['category/view', 'id' => $category->id]];

  if (count($categories) > 1) {
    $this->params['secondary_nav_items'] = $categories;
  }

  $product_images = $model->getImages();

  $js = <<<JS

  function resizeProductGal() {
    $('.product-images').css({
      'height': Math.max($(window).height() - $('.les-navbar').outerHeight() - $('footer').outerHeight() - 45, $('.product-description').outerHeight(), 700),
      'overflow-y': 'scroll'
    });

  };
  resizeProductGal();
  $(window).on('resize', function(){
    resizeProductGal();
  });
  $("a.single_image").fancybox();
JS;
  $this->registerJsFile('/js/jquery.elevateZoom-3.0.8.min.js', ['depends' => 'app\assets\AppAsset']);
  $this->registerJsFile('/js/fancybox/jquery.fancybox.pack.js', ['depends' => 'app\assets\AppAsset']);
  $this->registerCssFile('/js/fancybox/jquery.fancybox.css');
  $this->registerJs($js);

?>
<div class="product-view row clearfix">
  <div class="col-xs-6 product-images">
      <ul>
    <?php

      if ($product_images) {
      foreach ($product_images as $p_img) {?>
          <li>
            <a class="single_image" href="/<?=$p_img->getPath()?>">
              <img class="product-image" src="<?=$p_img->getPath('555x', ['abs' => true])?>" alt="<?=$model->sku_ru?>" data-orig-src="<?=$p_img->getPath('555x', ['abs' => true])?>" style="width:100%;"/>
            </a>
          </li>
    <?php }
      }

    ?>
      </ul>
  </div>
  <div class="col-xs-6 product-description" style="padding-left:30px;">
        <?=Breadcrumbs::widget([
  'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
])?>
    <div class="description">
      <div class="title space-bottom-medium">
        <h2 class="text-uppercase"><?=$model->{Yii::t('site', 'model_en')}?></h2>
      </div>
      <div class="product-description">
        <?=ProductDescription::widget(['model' => $model])?>
      </div>

      <div class="specs">
        <div class="spec">
          <h3 class="text-uppercase"><?=Yii::t('site', 'Size')?></h3>
          <span class="size text-grey">
            <?=ProductDimensions::widget(['model' => $model])?>
          </span>
        </div>

        <div class="spec">
          <h3 class="text-uppercase"><?=Yii::t('site', 'Price')?></h3>
          <span class="price text-grey"><?=$model->price?> <span class="glyphicon glyphicon-ruble" style="font-size:11px;"/>.</span>
          &#x20;- <?=$model->status->{Yii::t('site', 'description_en')}?>
        </div>
      </div>

      <div class="cart-actions">
        <div class="action add-co-cart col-xs-6">
          <a class="text-center" href="<?=URL::to(['product/addtocart', 'id' => $model->id])?>"><?=Yii::t('site', 'Add to cart')?></a>
        </div>
        <div class="action buy-it-now col-xs-6">
          <a class="text-center" href="<?=URL::to(['product/buyitnow', 'id' => $model->id])?>"><?=Yii::t('site', 'Place an order')?></a>
        </div>
      </div>

    </div>
  </div>
</div>


<?php

if (!Yii::$app->user->isGuest): ?>

    <h1><?=Html::encode($this->title)?></h1>

    <p>
        <?=Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
        <?=Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
  'class' => 'btn btn-danger',
  'data'  => [
    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
    'method'  => 'post',
  ],
])?>
    </p>

    <?=DetailView::widget([
  'model'      => $model,
  'attributes' => [
    'id',
    'model_ru',
    'model_en',
    'sku_ru',
    'sku_en',
    'category.' . Yii::t('site', 'description_en'),
    'material.' . Yii::t('site', 'description_en'),
    'cut.' . Yii::t('site', 'description_en'),
    'price',
    'quantity',
    'length',
    'width_floor',
    'width_ceil',
    'height',
    // 'date_added:datetime',
    'sort_order',
  ],
])?>
<?php endif;?>


