
<?php
  use app\widgets\ProductDescription;
  use app\widgets\ProductDimensions;
  use yii\helpers\Url;

  /* @var $this yii\web\View */
  /* @var $model app\models\Product */
  $image = $model->getImage();
?>

<div class="category-product-preview"><a href="<?=Url::to(['product/view', 'id' => $model->id])?>"><img src="<?=YII_ENV_DEV ? '//lorempixel.com/540/' . rand(3,5). '00/' : $image->getPath('540x', ['abs' => true]);?>" alt="<?=$model->sku_ru;?>"/></a>
  <div class="category-product-preview__description"><?=ProductDescription::widget(['model' => $model])?>
    <div class="specs">
      <div class="spec">
        <h3 class="text-uppercase"></h3><span class="size text-grey"><?=ProductDimensions::widget(['model' => $model])?></span>
      </div>
      <div class="spec">
        <h3 class="text-uppercase"></h3><span class="price text-grey"><?=$model->price?>&#x20;<span class="glyphicon glyphicon-ruble" style="font-size:11px;"></span>. - <?=$model->status->{Yii::t('site', 'description_en')}?></span>
      </div>
    </div><br/>
    <div class="category-product-preview__cart-actions">
      <div class="action buy-it-now"><a class="text-center" href="<?=Url::to(['product/view', 'id' => $model->id])?>"><?=Yii::t('site', 'Details')?></a></div>
    </div>
  </div>
</div>