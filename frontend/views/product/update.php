<?php

  use yii\helpers\Html;

  /* @var $this yii\web\View */
  /* @var $model app\models\Product */

  $this->title = Yii::t('app', 'Update {modelClass}: ', [
    'modelClass' => 'Product',
  ]) . ' ' . $model->model_en;

  $this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
  $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
  $this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="product-update">

    <h1><?=Html::encode($this->title)?></h1>

	<div class="col-xs-12 product-images" style="margin-bottom: 20px;">
	    <?=$this->render('images', [
  'model' => $model,
])?>
	</div>

    <?=$this->render('_form', [
  'model' => $model,
])?>

</div>
<style>
	#dropZone {
		height:150px;
		width: 150px;
		float: left;
		margin-top: 2px;
		border: 1px solid black;
		background-repeat: no-repeat;
		background-position: center;
		background-image: url('http://dodobaror.com/images/plus_icon.png');
	}
	#dropZone.hover {
		border-color: cyan;
	}

	.product-images .ui-state-highlight {
	    float: left;
	    height: 150px;
	    width: 150px;
	    margin: 2px;
	}
	.product-images .product-image {
		border: 2px solid transparent;
	}
	.product-image.main-image {
		border: 2px solid green;
	}
	.product-image.main-image li.set-main-image {
		display: none;
	}
	.product-image li.set-main-image {
		display: list-item;
	}
</style>