<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yz\shoppingcart\ShoppingCart;

AppAsset::register($this);

$cart = new ShoppingCart();
// file_put_contents('data.txt', print_r($cart,1));

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=1200, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<link rel="shortcut icon" href="/favicon.ico?v=1" type="image/x-icon" />
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap" style="min-width:992px;">
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('/img/les_logo.png',['style' => 'height:48px;']),
        'brandUrl' => Yii::$app->homeUrl,
        'brandOptions' => [
	        'style' => 'height: 60px;',
        ],
        'options' => [
            'class' => 'navbar-default les-navbar',
        ],
    ]);
    echo Nav::widget([
        'options' => [
	        'class' => 'navbar-nav navbar-left nav-row-1',
        ],
        'items' => [
            ['label' => Yii::t('site', 'About'), 'url' => ['/site/about'], 'linkOptions' => ['class' => 'nav-row-1']],
            ['label' => Yii::t('site', 'Catalog'), 'url' => ['/site/furniture'], 'linkOptions' => ['class' => 'nav-row-1'], 'active' => (in_array(Yii::$app->controller->id, ['category', 'product'])) ? true : null],
//             ['label' => Yii::t('site', 'Technology'), 'url' => ['/site/technology']],
            ['label' => Yii::t('site', 'Contacts'), 'url' => ['/site/contact'], 'linkOptions' => ['class' => 'nav-row-1']],
			Yii::$app->user->isGuest ?
				'' :
				[
					'label' => Yii::t('site', 'Logout').' (' . Yii::$app->user->identity->username . ')',
					'url' => ['/site/logout'],
					'linkOptions' => ['data-method' => 'post', 'class' => 'nav-row-1'],
				],
        ],
    ]);
    echo Nav::widget([
        'options' => [
	        'class' => 'navbar-nav navbar-right',
        ],
        'items' => [
            '<li class="navbar-text" style="float:left; font-size:14px; letter-spacing: initial; top: -10px; right:-10px;">'.\lajax\languagepicker\widgets\LanguagePicker::widget([
				'itemTemplate' => '<a href="{link}" title="{language}">{name}</a>',
		        'activeItemTemplate' => '<a href="{link}" title="{language}" class="active">{name}</a>',
		        'parentTemplate' => '<div class="language-picker button-list {size}"><div>{items}<span style="float:left; font-size:1.5em; margin-top:-5px;">i</span>{activeItem}</div></div>',
		        'languageAsset' => false,
		        'size' => \lajax\languagepicker\widgets\LanguagePicker::SIZE_LARGE
		    ]) .'</li>',
            [
	            'label' => Yii::t('site', 'Cart') .($cart->getCount() ? Html::tag('span', $cart->getCount(), ['class' => 'cart-count']) : ''),
	            'url' => ['cart/view'],
	            'linkOptions' => [
		            'style' => 'padding-right: 60px;',
	            ],
	            'options' => [
		            'class' => 'cart-icon',
	            ],
            ],
        ],
        'encodeLabels' => false,
    ]);
    
    
    if (isset($this->params['secondary_nav_items'])) {
	    echo Nav::widget([
	        'options' => [
		        'class' => 'navbar-nav navbar-left small nav-row-2',
		        'style' => 'position: absolute; top: 66px; margin-left: 222px;',
	        ],
	        'items' => $this->params['secondary_nav_items'],
	    ]);
    }
	NavBar::end();
    ?>

    <div class="container">
<!--
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
-->
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
	    <div class="row">
        	<p class="pull-left">&copy; LES FURNITURE <?= date('Y') ?></p>
	    </div>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
