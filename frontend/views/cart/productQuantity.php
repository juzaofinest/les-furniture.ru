<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yz\shoppingcart\ShoppingCart;
use yii\jui\Spinner;

$cart = new ShoppingCart();
$id = $model->id;
$updatePositionUrl = Url::to(['cart/updateposition']);
?>

<?= Spinner::widget([
	'id' => 'quantity' .$id,
    'name'  => 'quantity',
    'value' => $model->getQuantity(),
    'options' => [
	    'style' => 'width: 2em;',
	    'readonly' => 'true',
    ],
    'clientOptions' => [
	    'step' => 1,
	    'min' => 0,
	    'numberFormat' => 'n',
    ],
    'clientEvents' => [
	    'stop' => 'function () {
		    if ($(this).val() == ' .$model->getQuantity() .') {
			    $("#updatePosition' .$id .'").addClass("hidden");
		    } else {
			    $("#updatePosition' .$id .'").removeClass("hidden");
		    }
	    }',
    ],
]) ?>
<a id="updatePosition<?= $id ?>" href="#" class="hidden" onclick="updatePosition(<?= $id ?>)"><span class="glyphicon glyphicon-ok"></span></a>
<script>
	function updatePosition(id) {
		$.get('<?= Url::to(['cart/updateposition'])?>', {
			id: id,
			q: $('#quantity' + id).val()
		});
	}
</script>