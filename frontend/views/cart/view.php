<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yz\shoppingcart\ShoppingCart;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('site', 'Cart');
$this->params['breadcrumbs'][] = $this->title;
Yii::$app->formatter->thousandSeparator = ' ';
?>
<div class="site-about">
	<h1 class="text-uppercase letter-spacing-medium"><?= Html::encode($this->title) ?></h1>
	
	<?= GridView::widget([
		'dataProvider' => $positionsDataProvider,
		'options' => [
			'class' => 'les-cart',
		],
		'tableOptions' => [
			'class' => 'table text-center',
		],
		'headerRowOptions' => [
			'class' => 'text-uppercase font-normal letter-spacing-medium',
		],
		'rowOptions' => [
			'class' => 'text-grey',
		],
		'showFooter' => true,
		'summary' => false,
		'columns' => [
/*
			[
				'header' => false,
				'class' => 'yii\grid\SerialColumn',
				'footer' => Yii::t('site', 'Total'),
			],
*/
			[
				'label' => Yii::t('site', 'Item'),
				'format' => 'html',
				'value' => function($model){ return '<img src="' .$model->getImage()->getPath('150x150', ['fit' => true, 'abs' => true]) .'"/>'; },
				'footer' => Html::a(Yii::t('site', 'Clear cart'), ['cart/removeall'], ['class' => 'xsmall pull-left']),
			],
			[
				'label' => Yii::t('site', 'Model'),
				'value' => function($model){ return $model->{Yii::t('site', 'model_en')}; },
			],
			[
				'label' => Yii::t('site', 'QTY'),
				'format' => 'raw',
				'value' => function($model){ return $this->render('/cart/productQuantity', [
					'p_img' => $model->getImage(),
					'model' => $model,
					]); },
			],
			[
				'label' => Yii::t('site', 'Unit price'),
				'value' => function($model){ return Yii::$app->formatter->asDecimal($model->price); },
			],
			[
				'label' => Yii::t('site', 'Cost'),
				'value' => function($model){ return Yii::$app->formatter->asDecimal($model->price * $model->getQuantity()); },
				'footer' => Yii::t('site', 'Total') .': ' .Yii::$app->formatter->asDecimal($cart->getCost()),
			],
//			 ['class' => 'yii\grid\ActionColumn'],
		],
	]); ?>
		
<?php if($cart->getCount()) : ?>
	<div class="row col-xs-offset">
		<div id="checkout-form" class="product-form">
			
			<?php $form = ActiveForm::begin(['options' => [],'layout' => 'inline']); ?>
		
			<?= $form->field($cform, 'firstname', [
					'template' => '{input}',
					'options' => [
						'class' => 'col-xs-4',
					],
				])->textInput([
					'placeholder' => Yii::t('site', 'Name'),
					'class' => 'form-control col-xs-12',
				]) ?>
		
			<?= $form->field($cform, 'phone', [
					'template' => '{input}',
					'options' => [
						'class' => 'col-xs-4',
					],
				])->widget(\yii\widgets\MaskedInput::className(), [
			    'mask' => '+7 (999) 999-9999',
			    'options' => [
				    'placeholder' => Yii::t('site', 'Phone'),
					'class' => "form-control col-xs-12",
			    ],
			]) ?>
		
			<?= $form->field($cform, 'email', [
					'template' => '{input}',
					'options' => [
						'class' => 'col-xs-4',
					],
				])->textInput([
					'placeholder' => Yii::t('site', 'E-mail'),
					'class' => "form-control col-xs-12",
				]) ?>
		
<!-- 			<?= $form->field($cform, 'payment_method')->inline()->radioList(['cash' => 'НАЛ', 'wire' => 'ОБНАЛ', 'card' => 'МАСТЕРКАРД']) ?> -->
			
			<div class="col-xs-12 form-group text-center space-top-small">
                <?= Html::submitButton(Yii::t('site', 'Make order'), ['class' => 'les-button', 'name' => 'contact-button']) ?>
			</div>
		
			<?php ActiveForm::end(); ?>
		
		</div>
	</div>
<?php endif; ?>

</div>
