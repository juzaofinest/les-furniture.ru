<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yz\shoppingcart\ShoppingCart;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('site', 'Cart');
$this->params['breadcrumbs'][] = $this->title;

$message = [
	'ru' => 'Уважаемый ' .$firstname .', Ваш заказ был успешно размещен, наш менеджер свяжется с вами ближайшее время.',
	'en' => 'Dear ' .$firstname .', your order has been placed. Our manager will contact you ASAP.',
];
//!  Ниже ссылка на каталог и контакты
?>
<div class="cart-success">
	<h1>Успешно</h1>
	<p> Номер заказа: <?= $order_id ?> </p>
	<p><?= $message[Yii::$app->language]?></p>
	
</div>
