<?php
  use app\models\Cut;
  use app\models\Material;
  use yii\helpers\ArrayHelper;
  use yii\helpers\Url;
  use yii\widgets\ActiveForm;

  $js = <<<JS

  window.resetform = function() {
    var Form = $('#myForm');
    $('select', Form).each(function() { this.selectedIndex = 0 });
    $("button", Form).click();
  }
JS;
  $this->registerJs($js);
?>

<div class="product-filter">
  <?php $form = ActiveForm::begin([
      'id'      => 'myForm',
      'method'  => 'get',
      'action'  => Url::to(['category/view', 'id' => $filter->category_id]),
      'options' => [
        'data-pjax' => true,
        'enctype'   => 'multipart/form-data',
        'onchange'  => '$("button", this).click()',
      ]]);
    echo $form->field($filter, 'price_to')->dropDownList(
      $filter->pricesTo(),
      ['prompt' => Yii::t('site', 'Select') . '...'])
      ->label(Yii::t('site', 'Price to'))
    . $form->field($filter, 'cut')->dropDownList(
      ArrayHelper::map(Cut::all(), 'id', Yii::t('site', 'description_en')),
      ['prompt' => Yii::t('site', 'Select') . '...'])
      ->label(Yii::t('site', 'Cut'))
    . $form->field($filter, 'material')->dropDownList(
      ArrayHelper::map(Material::all(), 'id', Yii::t('site', 'description_en')),
      ['prompt' => Yii::t('site', 'Select') . '...'])
      ->label(Yii::t('site', 'Material'))
    . $form->field($filter, 'moisture')->dropDownList(
      [
        '1' => Yii::t('site', 'Natural'),
        '2' => Yii::t('site', 'Dry') . ' (<9%)',
      ],
      ['prompt' => Yii::t('site', 'Select') . '...'])
      ->label(Yii::t('site', 'Moisture'))
    . '<div onclick="window.resetform()" class="btn btn-default">Сбросить</div><br>'
    . $form->field($filter, 'length_from')->dropDownList(
      $filter->lengthsFrom(),
      ['prompt' => Yii::t('site', 'Select') . '...'])
      ->label(Yii::t('site', 'Length from'))
    . $form->field($filter, 'length_to')->dropDownList(
      $filter->lengthsTo(),
      ['prompt' => Yii::t('site', 'Select') . '...'])
      ->label(Yii::t('site', 'Length to'))
    . $form->field($filter, 'width_from')->dropDownList(
      $filter->widthsFrom(),
      ['prompt' => Yii::t('site', 'Select') . '...'])
      ->label(Yii::t('site', 'Width from'))
    . $form->field($filter, 'width_to')->dropDownList(
      $filter->widthsTo(),
      ['prompt' => Yii::t('site', 'Select') . '...'])
      ->label(Yii::t('site', 'Width to'))

    . $form->field($filter, 'thickness')->dropDownList(
      $filter->thicknesses(),
      ['prompt' => Yii::t('site', 'Select') . '...'])
      ->label(Yii::t('site', 'Thickness'))

    ;
    echo '<button type="submit" style="display:none;"></button>';
    ActiveForm::end();
  ?>
</div>
<style>
.product-filter .form-group {
  display: inline-block;
  width: 18%;
  margin-right: 2%;
}

</style>