<?php
  use yii\grid\GridView;
  use yii\helpers\Html;
  use yii\widgets\ListView;
  use yii\widgets\Pjax;

  /* @var $this yii\web\View */
  /* @var $model app\models\Category */

  $this->title = $model->{Yii::t('site', 'description_en')};
  $this->params['breadcrumbs'][] = ['label' => Yii::t('site', 'Catalog'), 'url' => ['site/catalog']];

  //$this->params['breadcrumbs'][] = $this->title;
  if (count($categories) > 1) {
    $this->params['secondary_nav_items'] = $categories;
  }

  //print_r($categories);
?>

<?php Pjax::begin();?>
<?=$this->render('_product_filter.php', ['filter' => $filter])?>


<?=ListView::widget([
  'dataProvider' => $products,
  'layout'       => '{items}',
  'itemView'     => '/product/category_preview',
  'itemOptions'  => ['tag' => false],
  'options'      => ['class' => 'clearfix category-view'],
]);?>
<?php Pjax::end();?>


<?php

if (!Yii::$app->user->isGuest): ?>

  <p>
    <?=Html::a(Yii::t('app', 'Добавить товар'), ['product/create', 'category_id' => $model->id], ['class' => 'btn btn-primary', 'data-method' => 'post'])?>
    <?=Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary'])?>
    <?=Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
  'class' => 'btn btn-danger',
  'data'  => [
    'confirm' => Yii::t('app', 'Are you sure you want to delete this item?'),
    'method'  => 'post',
  ],
])?>
  </p>

  <?=GridView::widget([
  'dataProvider' => $products,
  'columns'      => [
    ['class' => 'yii\grid\SerialColumn'],
    //'id',
    [
      'label'  => 'Images',
      'format' => 'html',
      'value'  => function ($model) {return '<img src="' . $model->getImage()->getPath('150x150', ['abs' => true]) . '"/>';},
    ],
    'model' . Yii::t('site', '_en'),
    'sku' . Yii::t('site', '_en'),
    [
      'label' => 'Category',
      'value' => 'category.description' . Yii::t('site', '_en'),
    ],
    'price',
    // 'quantity',
    'length',
    // 'height',
    // 'sort_order',

    [
      'class'      => 'yii\grid\ActionColumn',
      'controller' => 'product',
    ],
  ],
]);?>
<?php endif;?>
